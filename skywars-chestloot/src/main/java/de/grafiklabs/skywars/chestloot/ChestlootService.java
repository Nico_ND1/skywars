package de.grafiklabs.skywars.chestloot;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.security.SecureRandom;
import java.util.*;

/**
 * @author Nico_ND1
 */
public class ChestlootService {

    private final Random random = new SecureRandom();
    private ChestlootConfig chestlootConfig = ChestlootConfig.fromFile("plugins/SkyWars/chestloot.json");
    private ChestlootConfig middleChestlootConfig = ChestlootConfig.fromFile("plugins/SkyWars/middleChestloot.json");

    public void fillChest(Inventory inventory, boolean middle) {
        final Map<ChestItemType, Integer> map = new HashMap<>();
        final ChestlootConfig config = middle ? this.middleChestlootConfig : this.chestlootConfig;

        config.getChestItemTypes().forEach(chestItemType -> {
            if (chestItemType.getProbability() == 0) {
                map.put(chestItemType, chestItemType.getMaxItems() == chestItemType.getMinItems() ?
                    chestItemType.getMaxItems() :
                    this.random.nextInt(chestItemType.getMaxItems() - chestItemType.getMinItems()) + chestItemType.getMinItems());
            } else {
                final double probability = chestItemType.getProbability() / 100;

                if (this.random.nextDouble() < probability)
                    map.put(
                        chestItemType,
                        chestItemType.getMaxItems() == chestItemType.getMinItems() ?
                            chestItemType.getMaxItems() :
                            this.random.nextInt(chestItemType.getMaxItems() - chestItemType.getMinItems()) + chestItemType.getMinItems()
                    );
            }
        });

        final List<ChestItem> list = new ArrayList<>();
        map.forEach((chestItemType, integer) -> {
            for (int i = 0; i < integer; i++) {
                final List<ChestItem> items = new ArrayList<>();
                chestItemType.getChestItems().forEach(chestItem -> {
                    for (int i1 = 0; i1 < chestItem.getProbability(); i1++) {
                        items.add(chestItem);
                    }
                });

                final ChestItem chestItem = items.get(new Random().nextInt(items.size()));
                list.add(chestItem);
            }
        });

        list.forEach(chestItem -> {
            int randomSlot = 0;
            while (inventory.getItem(randomSlot) != null)
                randomSlot = this.random.nextInt(inventory.getSize() - 1);

            final ItemStack itemStack = new ItemStack(
                chestItem.getMaterial(),
                chestItem.getMinAmount() == chestItem.getMaxAmount() ? chestItem.getMinAmount() : this.random.nextInt(chestItem.getMaxAmount() - chestItem.getMinAmount()) + chestItem.getMinAmount(),
                chestItem.getData());
            if (itemStack.getAmount() < 1) itemStack.setAmount(1);
            if (chestItem.getDamage() != null)
                itemStack.setDurability((short) (this.random.nextInt((chestItem.getDamage().getMaxDamage() - chestItem.getDamage().getMinDamage())) + chestItem.getDamage().getMinDamage()));
            if (chestItem.getEnchantment() != null)
                itemStack.addEnchantment(Enchantment.getByName(chestItem.getEnchantment().getType()), chestItem.getEnchantment().getLevel());

            inventory.setItem(randomSlot, itemStack);
        });
    }

    public void fillChest(Inventory inventory) {
        this.fillChest(inventory, false);
    }

    private int getRandom(int min, int max) {
        if (min == max) return min;

        return this.random.nextInt(max - min) + min;
    }

    public void reloadConfig() {
        this.chestlootConfig = ChestlootConfig.fromFile("plugins/SkyWars/chestloot.json");
        this.middleChestlootConfig = ChestlootConfig.fromFile("plugins/SkyWars/middleChestloot.json");
    }

}
