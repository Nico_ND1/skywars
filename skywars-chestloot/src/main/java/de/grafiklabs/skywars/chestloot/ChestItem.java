package de.grafiklabs.skywars.chestloot;
import com.google.gson.*;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.Material;

import java.lang.reflect.Type;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class ChestItem implements JsonDeserializer<ChestItem> {

    @Expose private final Material material;
    @Expose private final int minAmount;
    @Expose private final int maxAmount;
    @Expose private final short data;
    @Expose private final int probability; // 1 - 100
    @Expose private final Damage damage;
    @Expose private final Enchantment enchantment;
    private final UUID uuid; // So multiple chest items can be cointained in multiple chest item types

    @Override
    public ChestItem deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final Material material = Material.valueOf(jsonObject.get("material").getAsString());
        final int minAmount = jsonObject.get("minAmount").getAsInt();
        final int maxAmount = jsonObject.get("maxAmount").getAsInt();
        final short data = jsonObject.get("data").getAsShort();
        final int probability = jsonObject.get("probability").getAsInt();
        final Damage damage = jsonObject.has("damage") ?
            jsonDeserializationContext.deserialize(jsonObject.get("damage"), ChestItemType.class) :
            null;
        final Enchantment enchantment = jsonObject.has("enchantment") ?
            jsonDeserializationContext.deserialize(jsonObject.get("enchantment"), Enchantment.class) :
            null;

        return new ChestItem(
            material,
            minAmount,
            maxAmount,
            data,
            probability,
            damage,
            enchantment,
            UUID.randomUUID()
        );
    }

    @AllArgsConstructor
    @NoArgsConstructor(force = true)
    @Data
    public static class Damage implements JsonDeserializer<Damage> {

        @Expose private final short minDamage;
        @Expose private final short maxDamage;

        @Override
        public Damage deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final JsonObject jsonObject = jsonElement.getAsJsonObject();

            return new Damage(jsonObject.get("minDamage").getAsShort(), jsonObject.get("maxDamage").getAsShort());
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor(force = true)
    @Data
    public static class Enchantment implements JsonDeserializer<Enchantment> {

        @Expose private final String type;
        @Expose private final int level;

        @Override
        public Enchantment deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final JsonObject jsonObject = jsonElement.getAsJsonObject();
            
            return new Enchantment(jsonObject.get("type").getAsString(), jsonObject.get("level").getAsInt());
        }
    }

}
