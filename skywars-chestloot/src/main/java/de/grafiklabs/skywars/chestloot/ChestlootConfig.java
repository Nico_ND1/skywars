package de.grafiklabs.skywars.chestloot;
import com.google.gson.*;
import com.google.gson.annotations.Expose;
import de.grafiklabs.skywars.chestloot.ChestItem.Enchantment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.Material;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class ChestlootConfig implements JsonDeserializer<ChestlootConfig> {

    private static final Gson GSON = new GsonBuilder()
        .registerTypeAdapter(ChestlootConfig.class, new ChestlootConfig())
        .serializeNulls()
        .setPrettyPrinting()
        .excludeFieldsWithoutExposeAnnotation()
        .create();

    @Expose private final List<ChestItemType> chestItemTypes;

    public static ChestlootConfig fromFile(String path) {
        final ChestlootConfig defaultConfig = new ChestlootConfig(Collections.singletonList(new ChestItemType(
            1,
            3,
            0.0,
            Collections.singletonList(new ChestItem(
                Material.WOOL,
                3,
                34,
                (short) 4,
                69,
                null,
                new Enchantment("KNOCKBACK", 1),
                null
            ))
        )));

        final File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdir();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try (FileWriter fileWriter = new FileWriter(file)) {
                fileWriter.write(GSON.toJson(defaultConfig, ChestlootConfig.class));
                fileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            return GSON.fromJson(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")), ChestlootConfig.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return defaultConfig;
    }

    @Override
    public ChestlootConfig deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final List<ChestItemType> chestItemTypes = new ArrayList<>();
        jsonObject.getAsJsonArray("chestItemTypes").forEach(jsonElement1 -> {
            chestItemTypes.add(jsonDeserializationContext.deserialize(jsonElement1, ChestItemType.class));
        });

        return new ChestlootConfig(chestItemTypes);
    }
}
