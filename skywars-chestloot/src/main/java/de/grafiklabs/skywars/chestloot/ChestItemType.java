package de.grafiklabs.skywars.chestloot;
import com.google.gson.*;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class ChestItemType implements JsonDeserializer<ChestItemType> {

    @Expose private final int minItems;
    @Expose private final int maxItems;
    @Expose private final double probability; // Either minItems and maxItems are used, or this.
    @Expose private final List<ChestItem> chestItems;

    @Override
    public ChestItemType deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final int minItems = jsonObject.get("minItems").getAsInt();
        final int maxItems = jsonObject.get("maxItems").getAsInt();
        final double probability = jsonObject.get("probability").getAsDouble();
        final List<ChestItem> chestItems = new ArrayList<>();
        jsonObject.getAsJsonArray("chestItems").forEach(jsonElement1 -> {
            chestItems.add(jsonDeserializationContext.deserialize(jsonElement1, ChestItem.class));
        });

        return new ChestItemType(
            minItems,
            maxItems,
            probability,
            chestItems
        );
    }
}
