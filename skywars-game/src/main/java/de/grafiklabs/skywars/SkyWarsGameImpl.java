package de.grafiklabs.skywars;
import de.dytanic.cloudnet.bridge.CloudServer;
import de.grafiklabs.scoreboard.TeamScoreboard;
import de.grafiklabs.scoreboard.title.NoneTitleAnimation;
import de.grafiklabs.skywars.SkyWarsStatsPlayer.StatsType;
import de.grafiklabs.skywars.command.StartCommand;
import de.grafiklabs.skywars.countdown.Countdown;
import de.grafiklabs.skywars.end.EndCountdownAdapter;
import de.grafiklabs.skywars.game.DeathListener;
import de.grafiklabs.skywars.game.PlayersListener;
import de.grafiklabs.skywars.game.PrepareCountdownAdapter;
import de.grafiklabs.skywars.game.ProtectionCountdownAdapter;
import de.grafiklabs.skywars.inventory.ClickInventoryListener;
import de.grafiklabs.skywars.kit.KitManager;
import de.grafiklabs.skywars.kit.KitStats;
import de.grafiklabs.skywars.listener.*;
import de.grafiklabs.skywars.lobby.LobbyCountdownAdapter;
import de.grafiklabs.skywars.lobby.LobbyListener;
import de.grafiklabs.skywars.lobby.LobbyPlayersCountdownAdapter;
import de.grafiklabs.skywars.map.Island;
import de.grafiklabs.skywars.map.IslandManager;
import de.grafiklabs.skywars.map.VotingManager;
import de.grafiklabs.skywars.team.Team;
import de.grafiklabs.skywars.team.TeamManager;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
@Data
public class SkyWarsGameImpl implements SkyWarsGame {

    private GameState gameState;
    private final SkyWarsConfig config;
    private final List<SkyWarsPlayer> players = new ArrayList<>();
    private final SkyWarsDatabase database;
    private final KitManager kitManager;
    private final TeamManager teamManager;
    private final VotingManager votingManager;
    private final IslandManager islandManager;
    private final ChestListener chestListener = new ChestListener();
    private Island island;
    private Island middleIsland;

    @Override
    public void setGameState(GameState newState) {
        final Plugin plugin = JavaPlugin.getPlugin(SkyWarsPlugin.class);
        HandlerList.unregisterAll(plugin);

        switch (newState) {
            case LOBBY:
                final Countdown lobbyCountdown = new Countdown(plugin, this.config.getInitialCountdown(), new LobbyCountdownAdapter(
                    this,
                    this.islandManager,
                    this.votingManager
                ));
                final Countdown playersCountdown = new Countdown(plugin, 30, new LobbyPlayersCountdownAdapter(this.config.getNeededPlayersToStart()));
                Bukkit.getPluginManager().registerEvents(new LobbyListener(
                    lobbyCountdown,
                    playersCountdown,
                    this,
                    this.kitManager,
                    this.teamManager,
                    this.votingManager,
                    this.islandManager,
                    this.config.getTeamSize(),
                    this.config
                ), plugin);
                playersCountdown.start();
                Bukkit.getPluginCommand("start").setExecutor(new StartCommand(this, lobbyCountdown, playersCountdown));

                Bukkit.getPluginManager().registerEvents(new DamageListener(), plugin);
                Bukkit.getPluginManager().registerEvents(new CancelListener(), plugin);
                break;
            case PREPARE:
                CloudServer.getInstance().changeToIngame();
                CloudServer.getInstance().updateAsync();

                final Countdown prepareCountdown = new Countdown(plugin, 33, new PrepareCountdownAdapter(this));
                prepareCountdown.start();

                this.teamManager.allocateTeamlessPlayers();
                this.teamManager.cleanUpEmptyTeams();
                final List<Vector> vectors = this.islandManager.generate(this.middleIsland, this.island, this.teamManager.getTeams().size(), this.island.getRadius());
                for (int i = 0; i < vectors.size(); i++) {
                    final Optional<Team> optionalTeam = this.teamManager.getTeam(i);
                    if (!optionalTeam.isPresent()) {
                        System.err.println("No team found for island #" + i);
                        continue;
                    }

                    final Team team = optionalTeam.get();
                    team.getSpawns().add(vectors.get(i).toLocation(Bukkit.getWorld("island")).add(.5, .1, .5));
                }

                Bukkit.getPluginManager().registerEvents(new DamageListener(), plugin);
                Bukkit.getPluginManager().registerEvents(this.chestListener, plugin);
                Bukkit.getPluginManager().registerEvents(new DeathListener(this, this.teamManager), plugin);

                final Random random = new Random();
                this.players.forEach(player -> {
                    if (player.getKit() == null) player.setKit(this.kitManager.getDefaultKit());
                    player.setLastKit(player.getKit());
                    player.getPlayer().getInventory().clear();
                    player.getPlayer().getInventory().setArmorContents(null);
                    player.getKit().onStart(player.getPlayer());

                    player.getStatsPlayer().increment(StatsType.GAMES);
                    player.setIngame(true);

                    this.database.getKitStats(player.getPlayer().getUniqueId(), player.getKit().getName(), player::setKitStats);
                    this.teamManager.getTeam(player.getPlayer()).ifPresent(team -> {
                        if (team.getSpawns().isEmpty()) return;

                        final Location location = team.getSpawns().get(random.nextInt(team.getSpawns().size()));
                        player.getPlayer().teleport(location);
                    });

                    final TeamScoreboard teamScoreboard = new TeamScoreboard(player.getPlayer(), new NoneTitleAnimation(Messages.SCOREBOARD_TITLE.getMessage()));
                    teamScoreboard.set("empty1", "");
                    teamScoreboard.set("kills1", "§8» §7Kills§8:");
                    teamScoreboard.set("kills2", "§8× §d0");
                    teamScoreboard.set("empty2", "");
                    teamScoreboard.set("players1", "§8» §7Spieler§8:");
                    teamScoreboard.set("players2", "§8× §c" + this.getIngamePlayers().size());
                    teamScoreboard.set("empty3", "");
                    teamScoreboard.set("kit1", "§8» §7Kit§8:");
                    teamScoreboard.set("kit2", "§8× §a" + player.getKit().getName());
                    teamScoreboard.set("empty4", "");
                    teamScoreboard.update();
                    TeamScoreboard.setScoreboard(player.getPlayer(), teamScoreboard);
                });

                final Countdown protectionCountdown = new Countdown(plugin, 3, new ProtectionCountdownAdapter());
                protectionCountdown.start();

                Bukkit.getPluginManager().registerEvents(new PlayersListener(this, protectionCountdown), plugin);
                break;
            case INGAME:
                Bukkit.getPluginManager().registerEvents(new TeamListener(this.teamManager), plugin);
                Bukkit.getPluginManager().registerEvents(this.chestListener, plugin);
                Bukkit.getPluginManager().registerEvents(new DeathListener(this, this.teamManager), plugin);
                Bukkit.getPluginManager().registerEvents(new PlayersListener(this, null), plugin);
                break;
            case END:
                Bukkit.getPluginManager().registerEvents(new DamageListener(), plugin);
                Bukkit.getPluginManager().registerEvents(new CancelListener(), plugin);

                Bukkit.getOnlinePlayers().forEach(player -> player.teleport(this.config.getLobbyLocation().toLocation()));
                break;
        }

        Bukkit.getPluginManager().registerEvents(new SkyWarsListener(this), plugin);
        Bukkit.getPluginManager().registerEvents(new ClickInventoryListener(), plugin);

        this.gameState = newState;
    }

    @Override
    public List<SkyWarsPlayer> getIngamePlayers() {
        return this.players.stream()
            .filter(SkyWarsPlayer::isIngame)
            .collect(Collectors.toList());
    }

    @Override
    public SkyWarsPlayer getPlayer(Player player) {
        final Optional<SkyWarsPlayer> optionalSkyWarsPlayer = this.players.stream()
            .filter(skyWarsPlayer -> skyWarsPlayer.getPlayer().getUniqueId().equals(player.getUniqueId()))
            .findFirst();
        if (optionalSkyWarsPlayer.isPresent()) return optionalSkyWarsPlayer.get();

        final SkyWarsPlayer skyWarsPlayer = new SkyWarsPlayer(
            player,
            new ArrayList<>(),
            new SkyWarsStatsPlayer(player.getUniqueId()),
            this.kitManager.getDefaultKit(),
            this.kitManager.getDefaultKit(),
            null,
            0,
            0,
            null,
            null,
            false,
            null
        );
        this.database.loadPlayer(skyWarsPlayer);
        this.players.add(skyWarsPlayer);

        return skyWarsPlayer;
    }

    @Override
    public void unloadPlayer(Player player) {
        final SkyWarsPlayer skyWarsPlayer = this.getPlayer(player);
        this.database.savePlayer(skyWarsPlayer, true, () -> {
        });
        this.players.remove(skyWarsPlayer);
    }

    @Override
    public void checkEnd() {
        this.teamManager.cleanUpEmptyTeams();

        if (this.teamManager.getTeams().size() <= 1) {
            if (this.teamManager.getTeams().size() == 1) {
                final Team winnerTeam = this.teamManager.getTeams().get(0);
                winnerTeam.getMembers().forEach(player -> {
                    player.getStatsPlayer().increment(StatsType.WINS);
                    player.getStatsPlayer().increment(StatsType.POINTS, 5);
                    player.getKitStats().increment(KitStats.StatsType.WINS);
                    player.getKitStats().increment(KitStats.StatsType.POINTS, 5);
                });

                Bukkit.broadcastMessage(Messages.WINNERS.getMessage(
                    winnerTeam.getId(),
                    String.join(", ", winnerTeam.getMembers().stream()
                        .map(player -> player.getPlayer().getDisplayName())
                        .toArray(String[]::new))
                ));
            } else {
                Bukkit.broadcastMessage(Messages.WINNER_EMPTY.getMessage());
            }

            this.setGameState(GameState.END);

            final Countdown countdown = new Countdown(JavaPlugin.getPlugin(SkyWarsPlugin.class), 10, new EndCountdownAdapter(this, this.database));
            countdown.start();
        }
    }

    @Override
    public Location giveSpectatorEquip(Player player) {
        final SkyWarsPlayer skyWarsPlayer = this.getPlayer(player);
        if (this.gameState == GameState.INGAME || this.gameState == GameState.PREPARE) {
            skyWarsPlayer.setIngame(false);
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            player.getInventory().addItem(new ItemBuilder(Material.COMPASS)
                .setName(Messages.COMPASS_ITEM_NAME.getMessage())
                .toItemStack());
            player.setAllowFlight(true);
            player.setFlying(true);
            player.spigot().setCollidesWithEntities(false);
            this.players.forEach(all1 -> this.players.forEach(all2 -> {
                if (!all2.isIngame()) all1.getPlayer().hidePlayer(all2.getPlayer());
            }));

            return new Location(Bukkit.getWorld("island"), 0, 100, 0);
        }

        return this.config.getLobbyLocation().toLocation();
    }
}
