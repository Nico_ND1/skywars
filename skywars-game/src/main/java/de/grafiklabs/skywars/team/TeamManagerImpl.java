package de.grafiklabs.skywars.team;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsPlayer;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nico_ND1
 */
public class TeamManagerImpl implements TeamManager {

    @Getter private final List<Team> teams = new ArrayList<>();
    @Setter private SkyWarsGame game;

    @Override
    public Team findOrCreateEmptyTeam() {
        final Optional<Team> optionalTeam = this.teams.stream()
            .filter(team -> team.getMembers().isEmpty())
            .findFirst();
        if (!optionalTeam.isPresent()) {
            int id = this.teams.size();
            while (this.getTeam(id).isPresent()) id++;
            final Team team = new Team(id, new ArrayList<>());
            this.teams.add(team);
            return team;
        }
        return optionalTeam.get();
    }

    @Override
    public void cleanUpEmptyTeams() {
        this.teams.removeIf(team -> team.getMembers().isEmpty());
    }

    @Override
    public void allocateTeamlessPlayers() {
        Bukkit.getOnlinePlayers().stream()
            .filter(player -> !this.getTeam(player).isPresent())
            .forEach(player -> this.findOrCreateEmptyTeam().getMembers().add(this.game.getPlayer(player)));
    }

    @Override
    public Optional<Team> getTeam(int id) {
        return this.teams.stream()
            .filter(team -> team.getId() == id)
            .findFirst();
    }

    @Override
    public Optional<Team> getTeam(Player player) {
        return this.teams.stream()
            .filter(team -> team.getMembers().stream().anyMatch(skyWarsPlayer -> skyWarsPlayer.getPlayer().equals(player)))
            .findFirst();
    }

    @Override
    public List<SkyWarsPlayer> getPlayers(Team team) {
        return team.getMembers();
    }
}
