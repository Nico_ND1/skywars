package de.grafiklabs.skywars;
import de.grafiklabs.skywars.SkyWarsGame.GameState;
import de.grafiklabs.skywars.chestloot.ChestlootService;
import de.grafiklabs.skywars.command.StatsCommand;
import de.grafiklabs.skywars.database.MongoConfig;
import de.grafiklabs.skywars.database.SkyWarsDatabaseImpl;
import de.grafiklabs.skywars.island.EmptyWorldGenerator;
import de.grafiklabs.skywars.island.IslandManagerImpl;
import de.grafiklabs.skywars.kit.KitManager;
import de.grafiklabs.skywars.kit.KitManagerImpl;
import de.grafiklabs.skywars.lobby.voting.VotingManagerImpl;
import de.grafiklabs.skywars.map.IslandManager;
import de.grafiklabs.skywars.map.VotingManager;
import de.grafiklabs.skywars.team.TeamManagerImpl;
import org.bukkit.*;
import org.bukkit.plugin.ServicePriority;

/**
 * @author Nico_ND1
 */
public class SkyWarsPluginImpl extends SkyWarsPlugin {

    @Override
    public void onEnable() {
        {
            final WorldCreator worldCreator = new WorldCreator("island").generator(new EmptyWorldGenerator()).type(WorldType.FLAT);
            final World world = Bukkit.createWorld(worldCreator);
            world.setGameRuleValue("doDaylightCycle", "false");
            world.setGameRuleValue("doMobSpawning", "false");
            world.setThunderDuration(0);
            world.setThundering(false);
            world.setWeatherDuration(0);
            world.setStorm(false);
            world.setTime(6000);
            world.setDifficulty(Difficulty.EASY);
        }

        final ChestlootService chestlootService = new ChestlootService();
        Bukkit.getServicesManager().register(ChestlootService.class, chestlootService, this, ServicePriority.Normal);
        final SkyWarsDatabaseImpl database = new SkyWarsDatabaseImpl(MongoConfig.loadConfig());
        final KitManager kitManager = new KitManagerImpl(database.loadKits());
        database.setKitManager(kitManager);
        final TeamManagerImpl teamManager = new TeamManagerImpl();
        final SkyWarsConfig config = SkyWarsConfig.loadConfig();
        final VotingManager votingManager = new VotingManagerImpl();
        final IslandManager islandManager = new IslandManagerImpl(config);
        final SkyWarsGame game = new SkyWarsGameImpl(config, database, kitManager, teamManager, votingManager, islandManager);
        teamManager.setGame(game);
        game.setGameState(GameState.LOBBY);

        this.getCommand("stats").setExecutor(new StatsCommand(game, database));
    }
}
