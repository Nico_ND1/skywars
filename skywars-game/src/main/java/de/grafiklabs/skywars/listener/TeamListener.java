package de.grafiklabs.skywars.listener;
import de.grafiklabs.skywars.team.TeamManager;
import lombok.AllArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class TeamListener implements Listener {

    private final TeamManager teamManager;

    @EventHandler
    public void onDamageByEntity(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) return;

        final Player player = (Player) event.getEntity();
        final Player damager = (Player) event.getDamager();
        this.teamManager.getTeam(player).ifPresent(team -> {
            this.teamManager.getTeam(damager).ifPresent(team1 -> {
                if (team.getId() == team1.getId()) event.setCancelled(true);
            });
        });
    }

}
