package de.grafiklabs.skywars.listener;
import de.grafiklabs.skywars.chestloot.ChestlootService;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
public class ChestListener implements Listener {

    private final List<Location> chests = new ArrayList<>();
    private final List<Location> fakeChests = new ArrayList<>(); // Placed from player

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true) // Also needed in block break event
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if (event.getClickedBlock().getType() == Material.CHEST || event.getClickedBlock().getType() == Material.TRAPPED_CHEST) {
            final Chest chest = (Chest) event.getClickedBlock().getState();

            if (this.fakeChests.contains(chest.getLocation())) return;
            if (!this.chests.contains(chest.getLocation())) {
                this.chests.add(chest.getLocation());

                final ChestlootService chestlootService = Bukkit.getServicesManager().load(ChestlootService.class);
                chestlootService.fillChest(chest.getBlockInventory(), event.getClickedBlock().getType() == Material.TRAPPED_CHEST);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getBlock().getType() != Material.CHEST && event.getBlock().getType() != Material.TRAPPED_CHEST)
            return;

        if (!this.fakeChests.contains(event.getBlock().getLocation()) && !this.chests.contains(event.getBlock().getLocation())) {
            this.chests.add(event.getBlock().getLocation());

            final Chest chest = (Chest) event.getBlock().getState();
            final ChestlootService chestlootService = Bukkit.getServicesManager().load(ChestlootService.class);
            chestlootService.fillChest(chest.getBlockInventory(), event.getBlock().getType() == Material.TRAPPED_CHEST);
        }
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getBlock().getType() == Material.CHEST || event.getBlock().getType() == Material.TRAPPED_CHEST)
            this.fakeChests.add(event.getBlock().getLocation());
    }

}
