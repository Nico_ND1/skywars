package de.grafiklabs.skywars.listener;
import de.grafiklabs.scoreboard.TeamScoreboard;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsGame.GameState;
import de.grafiklabs.skywars.SkyWarsPlugin;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class SkyWarsListener implements Listener {

    private final SkyWarsGame game;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        if (this.game.getGameState() == GameState.LOBBY || this.game.getGameState() == GameState.END) return;
        if (!this.game.getPlayer(event.getPlayer()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        this.game.unloadPlayer(player);
        if (this.game.getGameState() == GameState.LOBBY || this.game.getGameState() == GameState.END) return;
        this.game.checkEnd();

        Bukkit.getScheduler().runTaskLater(JavaPlugin.getPlugin(SkyWarsPlugin.class), () -> {
            this.game.getPlayers().forEach(skyWarsPlayer -> {
                final TeamScoreboard teamScoreboard = TeamScoreboard.getScoreboard(skyWarsPlayer.getPlayer());
                teamScoreboard.set("players2", "§8× §c" + this.game.getIngamePlayers().size());
                teamScoreboard.update();
            });
        }, 10);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!this.game.getPlayer(event.getPlayer()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (!this.game.getPlayer(event.getPlayer()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (!this.game.getPlayer(event.getPlayer()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        if (!this.game.getPlayer(event.getPlayer()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent event) {
        if (!this.game.getPlayer(event.getPlayer()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (!this.game.getPlayer((Player) event.getEntity()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onDamageByEntity(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) return;
        if (!this.game.getPlayer((Player) event.getDamager()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent event) {
        if (!this.game.getPlayer((Player) event.getEntity()).isIngame()) event.setCancelled(true);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        event.setRespawnLocation(this.game.giveSpectatorEquip(event.getPlayer()));
    }

    @EventHandler
    public void onSpawn(PlayerSpawnLocationEvent event) {
        event.setSpawnLocation(this.game.giveSpectatorEquip(event.getPlayer()));
    }

    @EventHandler
    public void on(PlayerPortalEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void on(PlayerEditBookEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void on(PlayerAchievementAwardedEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void on(CreatureSpawnEvent event) {
        if (event.getSpawnReason() == SpawnReason.NATURAL) event.setCancelled(true);
        if (event.getEntity() instanceof Horse) {
            final Horse horse = (Horse) event.getEntity();
            if (!horse.isAdult()) {
                horse.setAdult();
                horse.setVariant(Variant.HORSE);
            }
        }
    }

    @EventHandler
    public void on(EntityCreatePortalEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

}
