package de.grafiklabs.skywars.lobby;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.countdown.CountdownAdapter;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class LobbyPlayersCountdownAdapter extends CountdownAdapter {

    private final int neededPlayers;

    @Override
    public void onFinish() {
        this.countdown.reset();
        this.countdown.start();
        if (!Bukkit.getOnlinePlayers().isEmpty())
            Bukkit.broadcastMessage(Messages.LOBBY_PLAYERS_COUNTDOWN.getMessage(this.neededPlayers - Bukkit.getOnlinePlayers().size()));
    }

    @Override
    public void onStop() {
        this.countdown.reset();
    }
}
