package de.grafiklabs.skywars.lobby.kit;
import de.grafiklabs.scoreboard.TeamScoreboard;
import de.grafiklabs.skywars.ItemBuilder;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.inventory.ClickInventory;
import de.grafiklabs.skywars.inventory.ClickItem;
import de.grafiklabs.skywars.kit.AbstractKit;
import de.grafiklabs.skywars.kit.KitManager;
import de.grafiklabs.skywars.kit.KitStats.StatsType;
import org.bukkit.Material;
import org.bukkit.Sound;

import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class KitSelectInventory extends ClickInventory {

    private final KitManager kitManager;
    private final AbstractKit kit;
    private final SkyWarsPlayer skyWarsPlayer;

    public KitSelectInventory(SkyWarsPlayer skyWarsPlayer, AbstractKit kit, KitManager kitManager) {
        super(skyWarsPlayer.getPlayer(), Messages.KIT_SELECT_INVENTORY.getMessage(), 9);
        this.skyWarsPlayer = skyWarsPlayer;
        this.kit = kit;
        this.kitManager = kitManager;

        this.updateInventory();
        this.open();
    }

    private void updateInventory() {
        final boolean hasKit = this.skyWarsPlayer.getKits().contains(this.kit.getName());
        this.setItem(8, new ClickItem(new ItemBuilder(Material.INK_SACK, 1, (byte) 10)
            .setName(hasKit ? Messages.KIT_SELECT_ITEM.getMessage() : Messages.KIT_BUY_ITEM.getMessage())
            .toItemStack(), () -> {
            if (hasKit) {
                this.skyWarsPlayer.setKit(this.kit);
                this.skyWarsPlayer.getPlayer().closeInventory();
                this.player.sendMessage(Messages.KIT_SELECTED.getMessage());
                this.player.playSound(this.player.getLocation(), Sound.WOOD_CLICK, 1, 1);

                final TeamScoreboard teamScoreboard = TeamScoreboard.getScoreboard(this.player);
                teamScoreboard.set("kit2", "§8× §a" + this.skyWarsPlayer.getKit().getName());
                teamScoreboard.update();
            } else {
                if (this.kit.getPrice() > this.skyWarsPlayer.getCoins()) {
                    this.player.sendMessage(Messages.KIT_BUY_NOT_ENOUGH_COINS.getMessage());
                } else {
                    this.skyWarsPlayer.setCoins(this.skyWarsPlayer.getCoins() - this.kit.getPrice());
                    this.skyWarsPlayer.setCoinsDiff(this.skyWarsPlayer.getCoinsDiff() - this.kit.getPrice());
                    this.skyWarsPlayer.getKits().add(this.kit.getName());
                    this.skyWarsPlayer.getPlayer().closeInventory();
                    this.player.sendMessage(Messages.KIT_BOUGHT.getMessage());
                    this.player.playSound(this.player.getLocation(), Sound.WOOD_CLICK, 1, 1);
                }
            }
        }));

        this.setItem(3, new ClickItem(new ItemBuilder(this.kit.getDisplayItem().clone())
            .setName(this.kit.getDisplayItem().getItemMeta().getDisplayName() +
                (this.skyWarsPlayer.getKits().contains(this.kit.getName()) ? " §a(Gekauft)" : " §c(Nicht gekauft)")).toItemStack(), null));
        this.setItem(5, new ClickItem(new ItemBuilder(Material.DIAMOND)
            .setName(Messages.KIT_SELECT_TOPLIST_ITEM.getMessage())
            .setLore(this.kit.getTopList().stream()
                .map(kitStats -> Messages.KIT_SELECT_TOPLIST_LORE.getMessage(
                    this.kit.getTopList().indexOf(kitStats) + 1,
                    kitStats.getName(),
                    kitStats.getMap().get(StatsType.KILLS),
                    kitStats.getMap().get(StatsType.WINS)
                )).collect(Collectors.toList()))
            .toItemStack(), null));

        this.setItem(0, new ClickItem(new ItemBuilder(Material.SLIME_BALL)
            .setName(Messages.KIT_SELECT_BACK.getMessage())
            .toItemStack(), () -> new KitInventory(this.skyWarsPlayer, this.kitManager)));
    }

}
