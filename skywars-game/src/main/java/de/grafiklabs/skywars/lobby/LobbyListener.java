package de.grafiklabs.skywars.lobby;
import de.grafiklabs.scoreboard.TeamScoreboard;
import de.grafiklabs.scoreboard.title.NoneTitleAnimation;
import de.grafiklabs.skywars.*;
import de.grafiklabs.skywars.countdown.Countdown;
import de.grafiklabs.skywars.kit.KitManager;
import de.grafiklabs.skywars.lobby.kit.KitInventory;
import de.grafiklabs.skywars.lobby.team.TeamInventory;
import de.grafiklabs.skywars.lobby.voting.VotingInventory;
import de.grafiklabs.skywars.map.IslandManager;
import de.grafiklabs.skywars.map.VotingManager;
import de.grafiklabs.skywars.team.TeamManager;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class LobbyListener implements Listener {

    private final Countdown lobbyCountdown;
    private final Countdown playersCountdown;
    private final SkyWarsGame game;
    private final KitManager kitManager;
    private final TeamManager teamManager;
    private final VotingManager votingManager;
    private final IslandManager islandManager;
    private final int teamSize;
    private final SkyWarsConfig config;

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        final Player player = event.getPlayer();
        this.game.getPlayer(player);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        event.setJoinMessage(Messages.JOIN.getMessage(player.getDisplayName()));

        player.getInventory().clear();
        player.getInventory().addItem(
            new ItemBuilder(Material.CHEST)
                .setName(Messages.KITS_ITEM.getMessage())
                .toItemStack(),
            new ItemBuilder(Material.BED)
                .setName(Messages.TEAMS_ITEM.getMessage())
                .toItemStack(),
            new ItemBuilder(Material.PAPER)
                .setName(Messages.VOTING_ITEM.getMessage())
                .toItemStack()
        );
        //player.teleport(this.config.getLobbyLocation().toLocation());

        if (this.config.getNeededPlayersToStart() <= Bukkit.getOnlinePlayers().size()) {
            this.lobbyCountdown.start();
            this.playersCountdown.stop();
        }
        if (Bukkit.getOnlinePlayers().size() <= 1) this.lobbyCountdown.reset();

        final SkyWarsPlayer skyWarsPlayer = this.game.getPlayer(player);
        if (skyWarsPlayer.getKit() != null) skyWarsPlayer.setKit(skyWarsPlayer.getKit());

        final TeamScoreboard teamScoreboard = new TeamScoreboard(player, new NoneTitleAnimation(Messages.SCOREBOARD_TITLE.getMessage()));
        teamScoreboard.set("empty1", "");
        teamScoreboard.set("players1", "§8» §7Spieler§8:");
        teamScoreboard.set("players2", "§8× §c" + Bukkit.getOnlinePlayers().size());
        teamScoreboard.set("empty2", "");
        teamScoreboard.set("kit1", "§8» §7Kit§8:");
        teamScoreboard.set("kit2", "§8× §a" + skyWarsPlayer.getKit().getName());
        teamScoreboard.set("empty3", "");
        teamScoreboard.update();
        TeamScoreboard.setScoreboard(player, teamScoreboard);

        this.game.getPlayers().forEach(skyWarsPlayer1 -> {
            final Player player1 = skyWarsPlayer1.getPlayer();
            final TeamScoreboard teamScoreboard1 = TeamScoreboard.getScoreboard(player1);
            teamScoreboard1.set("players2", "§8× §c" + this.game.getPlayers().size());
            teamScoreboard1.update();
        });
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(Messages.QUIT.getMessage(event.getPlayer().getDisplayName()));

        if (Bukkit.getOnlinePlayers().size() - 1 < this.config.getNeededPlayersToStart()) {
            this.lobbyCountdown.stop();
            this.playersCountdown.start();
        }
        if (Bukkit.getOnlinePlayers().size() <= 1) this.lobbyCountdown.reset();

        Bukkit.getScheduler().runTaskLater(JavaPlugin.getPlugin(SkyWarsPlugin.class), () -> {
            this.game.getPlayers().forEach(skyWarsPlayer -> {
                final Player player1 = skyWarsPlayer.getPlayer();
                final TeamScoreboard teamScoreboard1 = TeamScoreboard.getScoreboard(player1);
                teamScoreboard1.set("players2", "§8× §c" + this.game.getPlayers().size());
                teamScoreboard1.update();
            });
        }, 10);
    }

    @EventHandler
    public void onSpawn(PlayerSpawnLocationEvent event) {
        event.setSpawnLocation(this.config.getLobbyLocation().toLocation());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) return;
        if (event.getItem() == null) return;
        final Player player = event.getPlayer();

        if (event.getItem().getType() == Material.CHEST) {
            new KitInventory(this.game.getPlayer(player), this.kitManager);
        } else if (event.getItem().getType() == Material.BED) {
            new TeamInventory(this.game.getPlayer(player), this.teamManager, this.teamSize);
        } else if (event.getItem().getType() == Material.PAPER) {
            new VotingInventory(this.game.getPlayer(player), this.votingManager, this.game, this.islandManager);
        }
    }

}
