package de.grafiklabs.skywars.lobby.team;
import de.grafiklabs.skywars.ItemBuilder;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.inventory.ClickInventory;
import de.grafiklabs.skywars.inventory.ClickItem;
import de.grafiklabs.skywars.team.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class TeamInventory extends ClickInventory {

    private final SkyWarsPlayer skyWarsPlayer;
    private final TeamManager teamManager;
    private final int teamSize;

    public TeamInventory(SkyWarsPlayer skyWarsPlayer, TeamManager teamManager, int teamSize) {
        super(skyWarsPlayer.getPlayer(), Messages.TEAM_TITLE.getMessage(), 3 * 9);
        this.skyWarsPlayer = skyWarsPlayer;
        this.teamManager = teamManager;
        this.teamSize = teamSize;

        this.updateInventory();
        this.open();
    }

    private void updateInventory() {
        final AtomicInteger atomicInteger = new AtomicInteger();
        this.teamManager.getTeams().forEach(team -> {
            this.setItem(atomicInteger.getAndIncrement(), new ClickItem(new ItemBuilder(Material.BED)
                .setName(Messages.TEAM_ITEM.getMessage(team.getId()))
                .setLore(team.getMembers().stream().map(player -> player.getPlayer().getDisplayName()).collect(Collectors.toList()))
                .toItemStack(), () -> {
                if (team.getMembers().size() >= this.teamSize) {
                    this.player.sendMessage(Messages.TEAM_FULL.getMessage());
                } else {
                    this.teamManager.getTeam(this.player).ifPresent(team1 -> team1.getMembers().remove(this.skyWarsPlayer));
                    team.getMembers().add(this.skyWarsPlayer);
                    this.teamManager.cleanUpEmptyTeams();
                    this.updateAll();
                }
            }));
        });
        this.setItem(atomicInteger.getAndIncrement(), new ClickItem(new ItemBuilder(Material.BED)
            .setName(Messages.TEAM_CREATE_ITEM.getMessage())
            .toItemStack(), () -> {
            this.teamManager.getTeam(this.player).ifPresent(team -> team.getMembers().remove(this.skyWarsPlayer));
            this.teamManager.findOrCreateEmptyTeam().getMembers().add(this.skyWarsPlayer);
            this.teamManager.cleanUpEmptyTeams();
            this.updateAll();
        }));
    }

    private void updateAll() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (player.getOpenInventory() == null || player.getOpenInventory().getTopInventory() == null) return;
            if (!player.getOpenInventory().getTopInventory().getName().equals(Messages.TEAM_TITLE.getMessage()))
                return;

            ((TeamInventory) ClickInventory.getInventory(player)).updateInventory();
        });
    }

}
