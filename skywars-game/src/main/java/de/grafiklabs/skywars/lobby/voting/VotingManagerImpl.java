package de.grafiklabs.skywars.lobby.voting;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.map.Island;
import de.grafiklabs.skywars.map.IslandManager;
import de.grafiklabs.skywars.map.VotingManager;

import java.util.Comparator;

/**
 * @author Nico_ND1
 */
public class VotingManagerImpl implements VotingManager {
    @Override
    public Island poll(SkyWarsGame game, IslandManager islandManager, boolean middle) {
        return islandManager.getIslands(middle).stream()
            .max(Comparator.comparingInt(value -> this.getVotes(game, value)))
            .orElse(islandManager.getIslands(middle).get(0));
    }

    @Override
    public int getVotes(SkyWarsGame game, Island island) {
        return (int) game.getPlayers().stream()
            .filter(player -> island.equals(player.getVotedMiddleIsland()) || island.equals(player.getVotedIsland()))
            .count();
    }

    @Override
    public boolean vote(SkyWarsPlayer player, Island island, boolean middle) {
        if (middle) {
            if (!island.equals(player.getVotedMiddleIsland())) {
                player.setVotedMiddleIsland(island);
                return true;
            }
        } else {
            if (!island.equals(player.getVotedIsland())) {
                player.setVotedIsland(island);
                return true;
            }
        }
        return false;
    }
}
