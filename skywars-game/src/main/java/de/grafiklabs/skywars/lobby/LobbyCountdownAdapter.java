package de.grafiklabs.skywars.lobby;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsGame.GameState;
import de.grafiklabs.skywars.SkyWarsGameImpl;
import de.grafiklabs.skywars.countdown.CountdownAdapter;
import de.grafiklabs.skywars.map.Island;
import de.grafiklabs.skywars.map.IslandManager;
import de.grafiklabs.skywars.map.VotingManager;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Material;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class LobbyCountdownAdapter extends CountdownAdapter {

    private final SkyWarsGame game;
    private final IslandManager islandManager;
    private final VotingManager votingManager;

    @Override
    public void onStart() {
        Bukkit.broadcastMessage(Messages.LOBBY_COUNTDOWN_START.getMessage());
    }

    @Override
    public void onStop() {
        Bukkit.broadcastMessage(Messages.LOBBY_COUNTDOWN_STOP.getMessage());
    }

    @Override
    public void onFinish() {
        this.game.setGameState(GameState.PREPARE);
    }

    @Override
    public void onTick(int previousTick) {
        if (previousTick % 10 == 0 || previousTick <= 5)
            Bukkit.broadcastMessage(Messages.LOBBY_COUNTDOWN_TICK.getMessage(previousTick == 1 ? "einer Sekunde" : previousTick + " Sekunden"));

        if (previousTick == 10) {
            final Island island = this.votingManager.poll(this.game, this.islandManager, false);
            final Island middleIsland = this.votingManager.poll(this.game, this.islandManager, true);

            Bukkit.broadcastMessage(Messages.LOBBY_VOTING_RESULT.getMessage(middleIsland.getName(), island.getName()));

            ((SkyWarsGameImpl) this.game).setIsland(island);
            ((SkyWarsGameImpl) this.game).setMiddleIsland(middleIsland);

            Bukkit.getOnlinePlayers().forEach(player -> player.getInventory().remove(Material.PAPER));

            this.islandManager.generateMiddle(middleIsland, island);
        }
    }

}
