package de.grafiklabs.skywars.lobby.kit;
import de.grafiklabs.skywars.ItemBuilder;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.inventory.ClickInventory;
import de.grafiklabs.skywars.inventory.ClickItem;
import de.grafiklabs.skywars.kit.KitManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Nico_ND1
 */
public class KitInventory extends ClickInventory {

    private final KitManager kitManager;
    private final SkyWarsPlayer skyWarsPlayer;

    public KitInventory(SkyWarsPlayer skyWarsPlayer, KitManager kitManager) {
        super(skyWarsPlayer.getPlayer(), Messages.KIT_INVENTORY.getMessage(), 6 * 9);
        this.skyWarsPlayer = skyWarsPlayer;
        this.kitManager = kitManager;

        this.updateInventory();
        this.open();
    }

    private void updateInventory() {
        final AtomicInteger atomicInteger = new AtomicInteger();
        this.kitManager.getKits().forEach(abstractKit -> {
            this.setItem(atomicInteger.getAndIncrement(), new ClickItem(new ItemBuilder(abstractKit.getDisplayItem().clone())
                .setName(abstractKit.getDisplayItem().getItemMeta().getDisplayName() +
                    (this.skyWarsPlayer.getKits().contains(abstractKit.getName()) ? " §a(Gekauft)" : " §c(Nicht gekauft)"))
                .toItemStack(), () -> new KitSelectInventory(this.skyWarsPlayer, abstractKit, this.kitManager)));
        });
    }

}
