package de.grafiklabs.skywars.lobby.voting;
import de.grafiklabs.skywars.ItemBuilder;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.inventory.ClickInventory;
import de.grafiklabs.skywars.inventory.ClickItem;
import de.grafiklabs.skywars.map.Island;
import de.grafiklabs.skywars.map.IslandManager;
import de.grafiklabs.skywars.map.VotingManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Nico_ND1
 */
public class VotingInventory extends ClickInventory {

    private static final int[] GLASSES = {0, 1, 2, 3, 5, 6, 7, 8, 9, 17, 18, 19, 20, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 44, 45, 46, 47, 48, 50, 51, 52, 53};

    private final VotingManager votingManager;
    private final SkyWarsGame game;
    private final SkyWarsPlayer skyWarsPlayer;
    private final PageHelper<Island> middleIslandPages;
    private final PageHelper<Island> islandPages;
    private int page;
    private int middlePage;

    public VotingInventory(SkyWarsPlayer skyWarsPlayer, VotingManager votingManager, SkyWarsGame game, IslandManager islandManager) {
        super(skyWarsPlayer.getPlayer(), Messages.LOBBY_VOTING_TITLE.getMessage(), 6 * 9);
        this.skyWarsPlayer = skyWarsPlayer;
        this.votingManager = votingManager;
        this.game = game;
        final List<Island> middleIslands = islandManager.getIslands(true);
        this.middleIslandPages = new PageHelper<>(5, middleIslands);
        final List<Island> islands = islandManager.getIslands(false);
        this.islandPages = new PageHelper<>(8, islands);

        this.updateInventory();
        this.open();
    }

    private void updateInventory() {
        final List<Island> middleIslands = this.middleIslandPages.getPage(this.middlePage);
        this.fillVoting(true, middleIslands, 49, 43, 37, new int[]{38, 39, 40, 41, 42});

        final List<Island> islands = this.islandPages.getPage(this.page);
        this.fillVoting(false, islands, 4, 16, 10, new int[]{11, 12, 13, 14, 15, 21, 22, 23});

        for (int slot : GLASSES)
            this.setItem(slot, new ClickItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), null));
    }

    private void fillVoting(boolean middle, List<Island> islands, int bookSlot, int nextPageSlot, int previousPageSlot, int[] votingSlots) {
        for (int i = 0; i < islands.size(); i++) {
            final Island island = islands.get(i);
            if (votingSlots.length < i) break;

            this.setItem(votingSlots[i], new ClickItem(new ItemBuilder(Material.PAPER)
                .setName(Messages.LOBBY_VOTING_ITEM.getMessage(island.getName()))
                .setLore(Messages.LOBBY_VOTING_LORE.getMessage(this.votingManager.getVotes(this.game, island)))
                .toItemStack(), () -> {
                if (this.votingManager.vote(this.skyWarsPlayer, island, middle)) {
                    this.updateAll();
                    this.player.sendMessage(Messages.LOBBY_VOTING_VOTED.getMessage(island.getName()));
                }

                this.updateAll();
            }));
        }

        this.setItem(bookSlot, new ClickItem(new ItemBuilder(Material.BOOK)
            .setName(middle ? Messages.LOBBY_VOTING_BOOK_MIDDLE.getMessage() : Messages.LOBBY_VOTING_BOOK.getMessage())
            .toItemStack(), null));
        this.setItem(nextPageSlot, new ClickItem(new ItemBuilder(Material.ARROW)
            .setName(Messages.NEXT_PAGE.getMessage())
            .toItemStack(), () -> {
            if (middle) {
                if (this.middlePage + 2 > this.middleIslandPages.getMaxPages()) {
                    this.player.sendMessage(Messages.PAGE_NOT_FOUND.getMessage());
                } else {
                    this.middlePage++;
                    this.updateInventory();
                }
            } else {
                if (this.page + 2 > this.islandPages.getMaxPages()) {
                    this.player.sendMessage(Messages.PAGE_NOT_FOUND.getMessage());
                } else {
                    this.page++;
                    this.updateInventory();
                }
            }
        }));
        this.setItem(previousPageSlot, new ClickItem(new ItemBuilder(Material.ARROW)
            .setName(Messages.PREVIOUS_PAGE.getMessage())
            .toItemStack(), () -> {
            if (middle) {
                if (this.middlePage - 1 < 0) {
                    this.player.sendMessage(Messages.PAGE_NOT_FOUND.getMessage());
                } else {
                    this.middlePage--;
                    this.updateInventory();
                }
            } else {
                if (this.page - 1 < 0) {
                    this.player.sendMessage(Messages.PAGE_NOT_FOUND.getMessage());
                } else {
                    this.page--;
                    this.updateInventory();
                }
            }
        }));
    }

    private void updateAll() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (player.getOpenInventory() == null || player.getOpenInventory().getTopInventory() == null) return;
            if (!player.getOpenInventory().getTopInventory().getName().equals(Messages.LOBBY_VOTING_TITLE.getMessage()))
                return;

            ((VotingInventory) ClickInventory.getInventory(player)).updateInventory();
        });
    }

}
