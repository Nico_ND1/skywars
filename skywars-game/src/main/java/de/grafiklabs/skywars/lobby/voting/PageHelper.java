package de.grafiklabs.skywars.lobby.voting;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
class PageHelper<T> {

    private final List<List<T>> pages;

    PageHelper(int items, List<T> pages) {
        this.pages = Lists.partition(pages, items);
    }

    List<T> getPage(int page) {
        if (page < 0 || page > this.pages.size()) return new ArrayList<>();
        return this.pages.get(page);
    }

    int getMaxPages() {
        return this.pages.size();
    }

}
