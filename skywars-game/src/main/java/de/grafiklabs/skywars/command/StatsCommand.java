package de.grafiklabs.skywars.command;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsDatabase;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsStatsPlayer;
import de.grafiklabs.skywars.SkyWarsStatsPlayer.StatsType;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class StatsCommand implements CommandExecutor {

    private final SkyWarsGame game;
    private final SkyWarsDatabase database;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        final Player player = (Player) sender;

        if (args.length > 1) {
            player.sendMessage(Messages.STATS_COMMAND_HELP.getMessage());
            return false;
        }

        final String name = args.length == 1 ? args[0] : player.getName();
        if (Bukkit.getPlayer(name) != null) {
            this.sendStats(player, name, this.game.getPlayer(Bukkit.getPlayer(name)).getStatsPlayer());
            return true;
        }
        this.database.getStatsPlayer(name, statsPlayer -> this.sendStats(player, name, statsPlayer));

        return false;
    }

    private void sendStats(Player player, String name, SkyWarsStatsPlayer statsPlayer) {
        if (statsPlayer == null) {
            player.sendMessage(Messages.STATS_COMMAND_PLAYER_NOT_FOUND.getMessage());
            return;
        }

        player.sendMessage(Messages.STATS_COMMAND.getMessage(
            name,
            statsPlayer.getStat(StatsType.KILLS),
            statsPlayer.getStat(StatsType.DEATHS),
            statsPlayer.getStat(StatsType.KILLS) == 0 ? "0.0" : this.round((double) statsPlayer.getStat(StatsType.KILLS) / (double) statsPlayer.getStat(StatsType.DEATHS)),
            statsPlayer.getStat(StatsType.GAMES),
            statsPlayer.getStat(StatsType.WINS) == 0 ? "0" : Math.round(100 * ((double) statsPlayer.getStat(StatsType.WINS) / (double) statsPlayer.getStat(StatsType.GAMES)))
        ));
    }

    private double round(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

}
