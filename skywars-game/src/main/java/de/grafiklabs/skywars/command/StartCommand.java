package de.grafiklabs.skywars.command;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsGame.GameState;
import de.grafiklabs.skywars.countdown.Countdown;
import lombok.AllArgsConstructor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class StartCommand implements CommandExecutor {

    private final SkyWarsGame game;
    private final Countdown countdown;
    private final Countdown playersCountdown;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return false;

        if (this.game.getGameState() != GameState.LOBBY) {
            sender.sendMessage(Messages.START_COMMAND_ALREADY_STARTED.getMessage());
            return false;
        }
        if (this.countdown.getCurrentTick() <= 11) {
            sender.sendMessage(Messages.START_COMMAND_USELESS.getMessage());
            return false;
        }

        if (!this.countdown.isStarted()) {
            this.countdown.start();
            this.playersCountdown.stop();
            return false;
        }

        this.countdown.setTime(11);
        sender.sendMessage(Messages.START_COMMAND_SUCCESS.getMessage());
        return false;
    }
}
