package de.grafiklabs.skywars.end;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsDatabase;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.countdown.CountdownAdapter;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class EndCountdownAdapter extends CountdownAdapter {

    private final SkyWarsGame game;
    private final SkyWarsDatabase database;

    @Override
    public void onTick(int previousTick) {
        if (previousTick == 10 || previousTick <= 5)
            Bukkit.broadcastMessage(Messages.SERVER_STOP_TICK.getMessage(previousTick == 1 ? "einer Sekunde" : previousTick + " Sekunden"));
    }

    @Override
    public void onFinish() {
        final KickQueue kickQueue = new KickQueue(this.game, this.database);
        kickQueue.poll();
    }
}
