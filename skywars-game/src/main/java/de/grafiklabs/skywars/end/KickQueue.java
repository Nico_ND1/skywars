package de.grafiklabs.skywars.end;
import de.grafiklabs.skywars.SkyWarsDatabase;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * @author Nico_ND1
 */
class KickQueue {

    private final Queue<SkyWarsPlayer> queue = new ArrayDeque<>();
    private final SkyWarsDatabase database;

    KickQueue(SkyWarsGame game, SkyWarsDatabase database) {
        this.queue.addAll(game.getPlayers());
        this.database = database;
    }

    void poll() {
        final SkyWarsPlayer poll = this.queue.poll();
        if (poll == null) {
            Bukkit.shutdown();
            return;
        }

        this.database.savePlayer(poll, true, () -> {
            poll.getPlayer().kickPlayer("");
            this.poll();
        });
    }
}
