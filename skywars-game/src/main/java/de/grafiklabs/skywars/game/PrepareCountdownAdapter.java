package de.grafiklabs.skywars.game;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsGame.GameState;
import de.grafiklabs.skywars.countdown.CountdownAdapter;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class PrepareCountdownAdapter extends CountdownAdapter {

    private final SkyWarsGame game;

    @Override
    public void onFinish() {
        Bukkit.broadcastMessage(Messages.GAME_PREPARE_COUNTDOWN_FINISH.getMessage());
        this.game.setGameState(GameState.INGAME);
    }

    @Override
    public void onTick(int previousTick) {
        if (previousTick % 10 == 0 || previousTick <= 5)
            Bukkit.broadcastMessage(Messages.GAME_PREPARE_COUNTDOWN_TICK.getMessage(previousTick == 1 ? "einer Sekunde" : previousTick + " Sekunden"));
    }
}
