package de.grafiklabs.skywars.game;
import de.grafiklabs.scoreboard.TeamScoreboard;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.SkyWarsPlugin;
import de.grafiklabs.skywars.SkyWarsStatsPlayer.StatsType;
import de.grafiklabs.skywars.kit.KitStats;
import de.grafiklabs.skywars.team.TeamManager;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class DeathListener implements Listener {

    private final SkyWarsGame game;
    private final TeamManager teamManager;

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        final Player player = event.getEntity();
        final SkyWarsPlayer skyWarsPlayer = this.game.getPlayer(player);
        skyWarsPlayer.getStatsPlayer().increment(StatsType.DEATHS);
        skyWarsPlayer.setIngame(false);

        Player killer = player.getKiller();
        if (skyWarsPlayer.getLastDamager() != null && skyWarsPlayer.getLastDamager().getPlayer().isOnline()) {
            killer = skyWarsPlayer.getLastDamager().getPlayer();
        }
        if (killer != null) {
            final SkyWarsPlayer skyWarsKiller = this.game.getPlayer(killer);
            skyWarsKiller.getStatsPlayer().increment(StatsType.KILLS);
            skyWarsKiller.getStatsPlayer().increment(StatsType.POINTS);
            skyWarsKiller.getKitStats().increment(KitStats.StatsType.KILLS);
            skyWarsKiller.getKitStats().increment(KitStats.StatsType.POINTS);

            Bukkit.broadcastMessage(Messages.KILL_KILLER.getMessage(player.getName(), killer.getName()));

            final TeamScoreboard teamScoreboard = TeamScoreboard.getScoreboard(killer);
            teamScoreboard.set("kills2", "§8× §d" + skyWarsKiller.getStatsPlayer().getDifference().getOrDefault(StatsType.KILLS, 0));
            teamScoreboard.update();
        } else {
            Bukkit.broadcastMessage(Messages.KILL_NO_KILLER.getMessage(player.getName()));
        }

        event.setDeathMessage(null);
        event.setDroppedExp(0);

        this.teamManager.getTeam(player).ifPresent(team -> team.getMembers().remove(skyWarsPlayer));
        this.teamManager.cleanUpEmptyTeams();
        Bukkit.broadcastMessage(Messages.TEAMS_REMAINING.getMessage(this.teamManager.getTeams().size()));
        PlayersInventory.updateAll();

        final TeamScoreboard teamScoreboard = TeamScoreboard.getScoreboard(player);
        teamScoreboard.set("players2", "§8× §c" + this.game.getIngamePlayers().size());
        teamScoreboard.update();

        this.game.checkEnd();
    }

    @EventHandler(ignoreCancelled = true)
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (!(event.getDamager() instanceof Player)) return;
        final Player player = (Player) event.getEntity();
        this.game.getPlayer(player).setLastDamager(this.game.getPlayer((Player) event.getDamager()));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Bukkit.getScheduler().runTaskLater(JavaPlugin.getPlugin(SkyWarsPlugin.class), PlayersInventory::updateAll, 10);
    }

}
