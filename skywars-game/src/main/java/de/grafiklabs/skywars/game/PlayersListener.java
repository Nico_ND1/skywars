package de.grafiklabs.skywars.game;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.countdown.Countdown;
import lombok.AllArgsConstructor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.*;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class PlayersListener implements Listener {

    private final SkyWarsGame game;
    private final Countdown protectionCountdown;

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if ((this.protectionCountdown != null && this.protectionCountdown.isStarted()) &&
            (event.getTo().getBlockX() != event.getFrom().getBlockX() || event.getTo().getBlockZ() != event.getFrom().getBlockZ())) {
            event.setCancelled(true);
            event.getPlayer().teleport(event.getFrom().getBlock().getLocation().clone().add(0.5, 0, 0.5));
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) return;
        if (event.getItem() == null) return;

        if (event.getItem().getType() == Material.COMPASS) {
            new PlayersInventory(event.getPlayer(), this.game);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);

        event.getPlayer().setAllowFlight(true);
        event.getPlayer().setFlying(true);
        event.getPlayer().spigot().setCollidesWithEntities(false);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        PlayersInventory.updateAll();
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        if (event.getCause() == TeleportCause.ENDER_PEARL) {
            final Player player = event.getPlayer();
            event.setCancelled(true);
            player.setNoDamageTicks(2);
            player.setFallDistance(0);
            player.teleport(event.getTo());
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getCause() == DamageCause.ENTITY_EXPLOSION || event.getCause() == DamageCause.BLOCK_EXPLOSION) {
            event.setDamage(4);
        }
    }

    @EventHandler
    public void onItemConsume(PlayerItemConsumeEvent event) {
        if (event.getItem() != null && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName() &&
            event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("Golden Head")) {
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 8 * 20, 1, true, true), true);
        }
    }

}
