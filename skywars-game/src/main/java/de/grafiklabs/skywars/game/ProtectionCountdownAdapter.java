package de.grafiklabs.skywars.game;
import de.grafiklabs.skywars.countdown.CountdownAdapter;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

/**
 * @author Nico_ND1
 */
public class ProtectionCountdownAdapter extends CountdownAdapter {

    @Override
    public void onTick(int previousTick) {
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.sendTitle(" ", "§a" + previousTick);
            player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1);
        });
    }

    @Override
    public void onFinish() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.sendTitle(" ", " ");
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
        });
    }
}
