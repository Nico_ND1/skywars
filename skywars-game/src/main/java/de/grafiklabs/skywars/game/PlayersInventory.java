package de.grafiklabs.skywars.game;
import de.grafiklabs.skywars.Messages;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.inventory.ClickInventory;
import de.grafiklabs.skywars.inventory.ClickItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Nico_ND1
 */
public class PlayersInventory extends ClickInventory {

    private final SkyWarsGame game;

    public PlayersInventory(Player player, SkyWarsGame game) {
        super(player, Messages.PLAYERS_INVENTORY_TITLE.getMessage(), 3 * 9);
        this.game = game;

        this.updateInventory();
        this.open();
    }

    private void updateInventory() {
        final AtomicInteger atomicInteger = new AtomicInteger();
        this.game.getPlayers().stream()
            .filter(SkyWarsPlayer::isIngame)
            .forEach(player -> {
                final ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                final SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
                skullMeta.setDisplayName(Messages.PLAYERS_ITEM.getMessage(player.getPlayer().getName()));

                try {
                    final Field profileField = skullMeta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(skullMeta, ((CraftPlayer) player.getPlayer()).getProfile());
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException exception) {
                    exception.printStackTrace();
                }
                itemStack.setItemMeta(skullMeta);

                this.setItem(atomicInteger.getAndIncrement(), new ClickItem(itemStack, () -> {
                    if (player.isIngame()) {
                        this.player.teleport(player.getPlayer().getLocation());
                        this.player.closeInventory();
                    } else {
                        this.player.sendMessage(Messages.PLAYERS_PLAYER_NOT_INGAME.getMessage());
                    }
                }));
            });
    }

    static void updateAll() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            if (player.getOpenInventory() == null || player.getOpenInventory().getTopInventory() == null) return;
            if (!player.getOpenInventory().getTopInventory().getName().equals(Messages.PLAYERS_INVENTORY_TITLE.getMessage()))
                return;

            ((PlayersInventory) ClickInventory.getInventory(player)).updateInventory();
        });
    }

}
