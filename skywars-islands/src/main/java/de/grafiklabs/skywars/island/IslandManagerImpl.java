package de.grafiklabs.skywars.island;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.transform.BlockTransformExtent;
import com.sk89q.worldedit.function.mask.ExistingBlockMask;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.transform.AffineTransform;
import de.grafiklabs.skywars.SkyWarsConfig;
import de.grafiklabs.skywars.island.VectorCalculation.Calculation;
import de.grafiklabs.skywars.map.Island;
import de.grafiklabs.skywars.map.IslandManager;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class IslandManagerImpl implements IslandManager {

    @Getter private final List<Island> islands = new ArrayList<>();

    public IslandManagerImpl(SkyWarsConfig config) {
        this.islands.addAll(config.getIslands());
    }

    @Override
    public List<Island> getIslands(boolean middle) {
        return this.islands.stream().filter(island -> island.isMiddle() == middle).collect(Collectors.toList());
    }

    @Override
    public void generateMiddle(Island middle, Island outer) {
        final VectorCalculation vectorCalculation = new VectorCalculation(middle.getClipboard(), outer.getClipboard(), 0, outer.getRadius());
        final World world = Bukkit.getWorld("island");
        final BukkitWorld bukkitWorld = new BukkitWorld(world);
        final Vector middleVector = vectorCalculation.getOrigin();
        final com.sk89q.worldedit.Vector middleWorldEditVector = new com.sk89q.worldedit.Vector(middleVector.getX(), middleVector.getY(), middleVector.getZ());
        paste(bukkitWorld, middle.getClipboard(), middleWorldEditVector);
    }

    @Override
    public List<Vector> generate(Island middle, Island outer, int islands, int radius) {
        final VectorCalculation vectorCalculation = new VectorCalculation(middle.getClipboard(), outer.getClipboard(), islands, radius);
        final List<Calculation> calculations = vectorCalculation.calculate();
        final World world = Bukkit.getWorld("island");
        calculations.forEach(calculation -> calculation.getVector().toLocation(world).add(0, -1, 0).getBlock().setType(Material.BEDROCK));
        final BukkitWorld bukkitWorld = new BukkitWorld(world);

        calculations.forEach(calculation -> {
            final com.sk89q.worldedit.Vector vector = new com.sk89q.worldedit.Vector(
                calculation.getVector().getX(),
                calculation.getVector().getY(),
                calculation.getVector().getZ()
            );
            final File file = new File("plugins/SkyWars/Islands", outer.getName() + ".schematic");
            try {
                final Clipboard copy = ClipboardFormat.SCHEMATIC.getReader(new FileInputStream(file)).read(bukkitWorld.getWorldData());
                final AffineTransform transform = new AffineTransform();
                if (calculation.getAngle() > 90)
                    transform.rotateX(180);
                else if (calculation.getAngle() < 0)
                    transform.rotateX(90);
                else if (calculation.getAngle() > -90)
                    transform.rotateX(-90);
                else
                    transform.rotateX(-180);

                paste(bukkitWorld, copy, vector, transform);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return calculations.stream()
            .map(Calculation::getVector)
            .collect(Collectors.toList());
    }

    private void paste(BukkitWorld bukkitWorld, Clipboard clipboard, com.sk89q.worldedit.Vector vector) {
        paste(bukkitWorld, clipboard, vector, new AffineTransform());
    }

    private void paste(BukkitWorld bukkitWorld, Clipboard clipboard, com.sk89q.worldedit.Vector vector,
                       AffineTransform transform) {
        final EditSession extent = WorldEdit.getInstance().getEditSessionFactory().getEditSession(bukkitWorld, -1);
        final Extent extent1 = new BlockTransformExtent(extent, transform, bukkitWorld.getWorldData().getBlockRegistry());
        final ForwardExtentCopy copy = new ForwardExtentCopy(clipboard, clipboard.getRegion(), clipboard.getOrigin(), extent1, vector);
        if (!transform.isIdentity()) copy.setTransform(transform);
        copy.setSourceMask(new ExistingBlockMask(clipboard));
        try {
            Operations.completeLegacy(copy);
        } catch (MaxChangedBlocksException e) {
            e.printStackTrace();
        }
        extent.flushQueue();
    }

}
