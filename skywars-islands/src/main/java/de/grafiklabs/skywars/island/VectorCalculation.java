package de.grafiklabs.skywars.island;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@RequiredArgsConstructor
class VectorCalculation {

    private final Clipboard middleIsland;
    private final Clipboard island;
    private final int islands;
    private final int radius;
    private int step;

    List<Calculation> calculate() {
        final Vector origin = this.getOrigin();
        final List<Calculation> list = new ArrayList<>();
        final int radius = this.radius + this.getHigherWidth();
        for (int i = 0; i < this.islands; i++) {
            final double inc = (2 * Math.PI) / this.islands;
            final double angle = this.step++ * inc;
            final Vector vector = new Vector(Math.cos(angle) * radius, 0, Math.sin(angle) * radius);

            list.add(new Calculation(origin.clone().add(vector), Math.sin(angle) * radius, this.islands));
        }

        return list;
    }

    private int getHigherWidth() {
        return this.island.getDimensions().getBlockX() > this.island.getDimensions().getBlockZ() ? this.island.getDimensions().getBlockX() : this.island.getDimensions().getBlockZ();
    }

    Vector getOrigin() {
        final Vector origin = new Vector(10, 0, 10);
        if (this.middleIsland != null && this.middleIsland.getMaximumPoint().getBlockY() > this.island.getMaximumPoint().getBlockY())
            origin.setY(10 + this.middleIsland.getMaximumPoint().getBlockY());
        else
            origin.setY(10 + this.island.getMaximumPoint().getBlockY());

        return origin;
    }

    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @Data
    public class Calculation {

        private final Vector vector;
        private final double angle;
        private final int islands;

    }

}
