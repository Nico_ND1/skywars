package de.grafiklabs.skywars.database;
import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import de.grafiklabs.rest.RestRequest;
import de.grafiklabs.skywars.SkyWarsDatabase;
import de.grafiklabs.skywars.SkyWarsPlayer;
import de.grafiklabs.skywars.SkyWarsStatsPlayer;
import de.grafiklabs.skywars.SkyWarsStatsPlayer.StatsType;
import de.grafiklabs.skywars.kit.AbstractKit;
import de.grafiklabs.skywars.kit.AbstractKit.Type;
import de.grafiklabs.skywars.kit.EquipmentKit;
import de.grafiklabs.skywars.kit.KitManager;
import de.grafiklabs.skywars.kit.KitStats;
import de.grafiklabs.skywars.kit.item.ItemLoader;
import lombok.Getter;
import lombok.Setter;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Nico_ND1
 */
public class SkyWarsDatabaseImpl implements SkyWarsDatabase {

    private final ExecutorService executorService = Executors.newFixedThreadPool(3, new ThreadFactory() {
        final AtomicInteger atomicInteger = new AtomicInteger();

        @Override
        public Thread newThread(Runnable r) {
            final Thread thread = new Thread(r);
            thread.setName("SkyWars Database #" + this.atomicInteger.incrementAndGet());
            thread.setDaemon(true);
            return thread;
        }
    });
    private final MongoCollection<Document> statsCollection;
    private final MongoCollection<Document> kitStatsCollection;
    @Getter private final MongoCollection<Document> kitsCollection;
    @Setter private KitManager kitManager;

    public SkyWarsDatabaseImpl(MongoConfig config) {
        final CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
            CodecRegistries.fromCodecs(new UuidCodec(UuidRepresentation.STANDARD)),
            MongoClient.getDefaultCodecRegistry()
        );
        final MongoClientOptions clientOptions = MongoClientOptions.builder()
            .codecRegistry(codecRegistry)
            .build();

        final MongoClient mongoClient = new MongoClient(
            new ServerAddress(config.getHost(), config.getPort()),
            Collections.singletonList(MongoCredential.createCredential(
                config.getUser(), config.getDatabase(), config.getPassword().toCharArray()
            )), clientOptions
        );
        this.statsCollection = mongoClient.getDatabase(config.getDatabase()).getCollection("player-stats-skywars");
        this.kitStatsCollection = mongoClient.getDatabase(config.getDatabase()).getCollection("kit-stats-skywars");
        this.kitsCollection = mongoClient.getDatabase(config.getDatabase()).getCollection("kits-skywars");
        Logger.getLogger("org.mongodb.driver").setLevel(Level.OFF);
    }

    @Override
    public void loadPlayer(SkyWarsPlayer player) {
        this.executorService.submit(() -> {
            final Document document = this.statsCollection.find(Filters.eq("uuid", player.getPlayer().getUniqueId().toString())).first();
            if (document == null) {
                final Document document1 = new Document("uuid", player.getPlayer().getUniqueId().toString());
                for (StatsType statsType : StatsType.values()) {
                    document1.append(statsType.name(), 0);
                    player.getStatsPlayer().getStats().put(statsType, 0);
                    document1.append("last-kit", this.kitManager.getDefaultKit().getName());
                }
                document1.append("kits", Collections.singletonList(this.kitManager.getDefaultKit().getName()));

                this.statsCollection.insertOne(document1);
                return;
            }

            player.setKit(this.kitManager.getKit(document.getString("last-kit")));
            player.setLastKit(player.getKit());
            document.get("kits", List.class).forEach(object -> player.getKits().add(object.toString()));

            final SkyWarsStatsPlayer statsPlayer = new SkyWarsStatsPlayer(player.getPlayer().getUniqueId());
            for (StatsType statsType : StatsType.values())
                statsPlayer.getStats().put(statsType, document.getInteger(statsType.name(), 0));

            player.setStatsPlayer(statsPlayer);
        });

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("uuid", player.getPlayer().getUniqueId().toString());
        jsonObject.put("values", "COINS");
        final RestRequest restRequest = new RestRequest(jsonObject, "/player/get");
        restRequest.requestAsync(result -> {
            if (result.containsKey("COINS")) player.setCoins(Integer.valueOf(result.get("COINS").toString()));
        });
    }

    @Override
    public void savePlayer(SkyWarsPlayer player, boolean async, Runnable runnable) {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("uuid", player.getPlayer().getUniqueId().toString());
        jsonObject.put("COINS", player.getCoinsDiff());
        player.setCoinsDiff(0);
        final RestRequest restRequest = new RestRequest(jsonObject, "/player/update");
        restRequest.requestAsync(result -> {
        });

        this.executorService.submit(() -> {
            final Document document = new Document();
            final Document incDocument = new Document();
            player.getStatsPlayer().getDifference().forEach((statsType, integer) -> incDocument.append(statsType.name(), integer));
            player.getStatsPlayer().getDifference().clear();
            if (!incDocument.isEmpty()) document.append("$inc", incDocument);

            final Document setDocument = new Document();
            setDocument.append("last-kit", player.getKit().getName());
            setDocument.append("kits", player.getKits());
            document.append("$set", setDocument);

            this.statsCollection.updateOne(Filters.eq("uuid", player.getPlayer().getUniqueId().toString()), document);

            runnable.run();

            if (player.getKitStats() != null) {
                final Document kitStatsIncDocument = new Document();
                player.getKitStats().getDiff().forEach((statsType, integer) -> {
                    kitStatsIncDocument.append(statsType.name().toLowerCase(), integer);
                });
                if (kitStatsIncDocument.isEmpty()) return;

                this.kitStatsCollection.updateOne(
                    Filters.and(Filters.eq("uuid", player.getPlayer().getUniqueId().toString()), Filters.eq("kit", player.getKit().getName())),
                    new Document("$inc", kitStatsIncDocument)
                );
            }
        });
    }

    @Override
    public void getStatsPlayer(String name, Consumer<SkyWarsStatsPlayer> consumer) {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("values", "UUID");

        final RestRequest restRequest = new RestRequest(jsonObject, "/player/get");
        restRequest.requestAsync(result -> {
            if ("".equals(result.get("UUID"))) {
                consumer.accept(null);
                return;
            }
            this.getStatsPlayer(UUID.fromString(result.get("UUID").toString()), consumer);
        });
    }

    @Override
    public void getStatsPlayer(UUID uuid, Consumer<SkyWarsStatsPlayer> consumer) {
        this.executorService.submit(() -> {
            final Document document = this.statsCollection.find(Filters.eq("uuid", uuid.toString()))
                .projection(Projections.exclude("kits", "last-kit"))
                .first();
            if (document == null) {
                consumer.accept(null);
            } else {
                final SkyWarsStatsPlayer statsPlayer = new SkyWarsStatsPlayer(uuid);
                for (StatsType statsType : StatsType.values())
                    statsPlayer.getStats().put(statsType, document.getInteger(statsType.name(), 0));

                consumer.accept(statsPlayer);
            }
        });
    }

    @Override
    public void getKitStats(UUID uuid, String kitName, Consumer<KitStats> consumer) {
        this.executorService.submit(() -> {
            final Document document = this.kitStatsCollection.find(
                Filters.and(Filters.eq("uuid", uuid.toString()), Filters.eq("kit", kitName))
            ).first();
            if (document == null) {
                consumer.accept(new KitStats(uuid, null));

                final Document document1 = new Document("uuid", uuid.toString()).append("kit", kitName);
                for (KitStats.StatsType statsType : KitStats.StatsType.values())
                    document1.append(statsType.name().toLowerCase(), 0);
                this.kitStatsCollection.insertOne(document1);
                return;
            }

            final KitStats kitStats = new KitStats(uuid, null);
            for (KitStats.StatsType statsType : KitStats.StatsType.values()) {
                if (document.containsKey(statsType.name().toLowerCase()))
                    kitStats.getMap().put(statsType, document.getInteger(statsType.name().toLowerCase()));
            }

            consumer.accept(kitStats);
        });
    }

    @Override
    public List<AbstractKit> loadKits() {
        final List<AbstractKit> kits = new ArrayList<>();
        this.kitsCollection.find().forEach((Block<Document>) document -> {
            final String kitName = document.getString("name");
            final int price = document.getInteger("price");
            final ItemStack displayItem = new ItemLoader().load(document.get("display-item", Document.class));
            final Type type = Type.valueOf(document.getString("type"));

            final FindIterable<Document> statsFindIterable = this.kitStatsCollection.find(Filters.eq("kit", kitName))
                .sort(Sorts.descending("points"))
                .limit(5)
                .cursorType(CursorType.NonTailable)
                .maxTime(10, TimeUnit.SECONDS);
            final MongoCursor<Document> cursor = statsFindIterable.iterator();
            final List<KitStats> kitStats = new ArrayList<>(5);
            while (cursor.hasNext()) {
                final Document statsDocument = cursor.next();

                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("uuid", statsDocument.getString("uuid"));
                jsonObject.put("values", "NAME");
                final RestRequest restRequest = new RestRequest(jsonObject, "/player/get");
                final JSONObject result = restRequest.request();

                final KitStats stats = new KitStats(UUID.fromString(statsDocument.getString("uuid")), result.get("NAME").toString());
                stats.getMap().put(KitStats.StatsType.KILLS, statsDocument.getInteger("kills"));
                stats.getMap().put(KitStats.StatsType.POINTS, statsDocument.getInteger("points"));
                stats.getMap().put(KitStats.StatsType.WINS, statsDocument.getInteger("wins"));
                kitStats.add(stats);
            }

            switch (type) {
                case EQUIPMENT:
                    final Map<Integer, ItemStack> items = new HashMap<>();
                    document.get("items", List.class).forEach(object -> {
                        final Document itemsDocument = (Document) object;
                        items.put(itemsDocument.getInteger("slot"), new ItemLoader().load(itemsDocument));
                    });
                    kits.add(new EquipmentKit(kitName, price, displayItem, items, kitStats));
                    break;
            }
        });
        /*if (kits.isEmpty()) {
            this.kitsCollection.insertOne(new Document("name", "Test")
                .append("price", 10)
                .append("display-item", new Document("type", Material.WOOD_SWORD.name())
                    .append("display-name", "Test Kit")
                    .append("lore", Arrays.asList("Hihi", "Ich bin ne Lore")))
                .append("type", Type.EQUIPMENT.name())
                .append("items", Arrays.asList(
                    new Document("slot", 0)
                        .append("type", Material.WOOD_SWORD.name())
                        .append("unbreakable", true)
                        .append("enchantments", Arrays.asList(
                            new Document("type", Enchantment.KNOCKBACK.getName()).append("level", 1),
                            new Document("type", Enchantment.DAMAGE_ALL.getName()).append("level", 2)
                        ))
                        .append("flags", Arrays.asList(ItemFlag.HIDE_ENCHANTS.name(), ItemFlag.HIDE_UNBREAKABLE.name())),
                    new Document("slot", 1)
                        .append("type", Material.COOKED_BEEF.name())
                        .append("amount", 16)
                ))
            );
            return this.loadKits();
        }*/
        return kits;
    }
}
