package de.grafiklabs.skywars.database;
import com.google.gson.*;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class MongoConfig implements JsonDeserializer<MongoConfig> {

    private static final Gson GSON = new GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation()
        .setPrettyPrinting()
        .serializeNulls()
        .registerTypeAdapter(MongoConfig.class, new MongoConfig())
        .create();

    public static MongoConfig loadConfig() {
        final File file = new File("plugins/SkyWars/mongo.json");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.getParentFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            saveConfig(new MongoConfig("host", 27017, "database", "user", "password"));
        }

        try {
            return GSON.fromJson(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")), MongoConfig.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveConfig(MongoConfig mongoConfig) {
        try (FileWriter fileWriter = new FileWriter(new File("plugins/SkyWars/mongo.json"))) {
            fileWriter.write(GSON.toJson(mongoConfig, MongoConfig.class));
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Expose private final String host;
    @Expose private final int port;
    @Expose private final String database;
    @Expose private final String user;
    @Expose private final String password;

    @Override
    public MongoConfig deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        return new MongoConfig(
            jsonObject.get("host").getAsString(),
            jsonObject.get("port").getAsInt(),
            jsonObject.get("database").getAsString(),
            jsonObject.get("user").getAsString(),
            jsonObject.get("password").getAsString()
        );
    }
}
