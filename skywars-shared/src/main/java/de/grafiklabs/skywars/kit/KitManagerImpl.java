package de.grafiklabs.skywars.kit;
import lombok.Getter;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
@Getter
public class KitManagerImpl implements KitManager {

    private final List<AbstractKit> kits;
    private final AbstractKit defaultKit;

    public KitManagerImpl(List<AbstractKit> kits) {
        this.kits = kits.stream()
            .sorted(Comparator.comparingInt(AbstractKit::getPrice))
            .collect(Collectors.toList());
        this.defaultKit = kits.stream()
            .filter(abstractKit -> abstractKit.name.equalsIgnoreCase("Starter"))
            .findFirst()
            .orElse(null);
    }

    @Override
    public AbstractKit getKit(String name) {
        return this.kits.stream()
            .filter(kit -> kit.getName().equalsIgnoreCase(name))
            .findFirst()
            .orElse(this.defaultKit);
    }
}
