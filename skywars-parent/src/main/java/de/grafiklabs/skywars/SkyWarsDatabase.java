package de.grafiklabs.skywars;
import de.grafiklabs.skywars.kit.AbstractKit;
import de.grafiklabs.skywars.kit.KitStats;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * Interface for database operations.
 *
 * @author Nico_ND1
 */
public interface SkyWarsDatabase {

    /**
     * Load player from database.
     *
     * @param player Initialized {@link SkyWarsPlayer}
     */
    void loadPlayer(SkyWarsPlayer player);

    /**
     * Save player to database.
     *
     * @param player   Cached {@link SkyWarsPlayer}
     * @param async    Whether it should be saved async or not
     * @param runnable {@code runnable#run()} will be called when the database operation is done
     */
    void savePlayer(SkyWarsPlayer player, boolean async, Runnable runnable);

    /**
     * Load the player's stats from the database from the name.
     *
     * @param name     The name of the player
     * @param consumer {@code consumer#accept()} will be called when the database operation is done
     */
    void getStatsPlayer(String name, Consumer<SkyWarsStatsPlayer> consumer);

    /**
     * Load the player's stats from the database from the {@link UUID uuid}.
     *
     * @param uuid     The uuid of the player
     * @param consumer {@code consumer#accept()} will be called when the database operation is done
     */
    void getStatsPlayer(UUID uuid, Consumer<SkyWarsStatsPlayer> consumer);

    /**
     * Load the player's stats for the kit with the {@code kitName kit-name}.
     *
     * @param uuid     The uuid of the player
     * @param kitName  The name of the kit
     * @param consumer {@code consumer#accept()} will be called when the database operation is done
     */
    void getKitStats(UUID uuid, String kitName, Consumer<KitStats> consumer);

    /**
     * Load all kits from the database.
     *
     * @return A {@link List<AbstractKit> list} with all {@link AbstractKit kits}
     */
    List<AbstractKit> loadKits();

}
