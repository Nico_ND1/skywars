package de.grafiklabs.skywars;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Data
public class SkyWarsStatsPlayer {

    private final UUID uuid;
    private final Map<StatsType, Integer> difference = new HashMap<>();
    private final Map<StatsType, Integer> stats = new HashMap<>();

    public int getStat(StatsType statsType) {
        return this.stats.getOrDefault(statsType, 0) + this.difference.getOrDefault(statsType, 0);
    }

    public void increment(StatsType statsType, int amount) {
        this.difference.put(statsType, this.difference.getOrDefault(statsType, 0) + amount);
    }

    public void increment(StatsType statsType) {
        this.increment(statsType, 1);
    }

    public enum StatsType {

        POINTS,
        KILLS,
        DEATHS,
        WINS,
        GAMES

    }

}
