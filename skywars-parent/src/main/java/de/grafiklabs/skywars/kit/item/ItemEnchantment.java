package de.grafiklabs.skywars.kit.item;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.enchantments.Enchantment;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Data
public class ItemEnchantment {

    private final Enchantment enchantment;
    private final int level;

}
