package de.grafiklabs.skywars.kit;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Data
public abstract class AbstractKit {

    protected final String name;
    protected final int price;
    protected final ItemStack displayItem;
    private final List<KitStats> topList;

    public abstract void onStart(Player player);

    public enum Type {

        EQUIPMENT

    }

}
