package de.grafiklabs.skywars.kit.item;
import org.bson.Document;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class ItemLoader {

    public static Document fromItemStack(ItemStack itemStack) {
        final Document document = new Document();

        final Material type = itemStack.getType();
        document.append("type", type.name());

        final int amount = itemStack.getAmount();
        if (amount != 1) document.append("amount", amount);

        final int data = itemStack.getData().getData();
        if (data != 0) document.append("data", data);

        final short durability = itemStack.getDurability();
        if (durability != itemStack.getType().getMaxDurability()) document.append("durability", durability);

        final ItemMeta itemMeta = itemStack.getItemMeta();
        final Set<ItemFlag> itemFlags = itemMeta.getItemFlags();
        if (itemFlags != null && !itemFlags.isEmpty()) {
            document.append("flags", itemFlags.stream()
                .map(Enum::name)
                .collect(Collectors.toList()));
        }

        final String displayName = itemMeta.getDisplayName();
        if (displayName != null) document.append("display-name", displayName);

        final List<String> lore = itemMeta.getLore();
        if (lore != null && !lore.isEmpty()) document.append("lore", lore);

        final boolean unbreakable = itemMeta.spigot().isUnbreakable();
        document.append("unbreakable", unbreakable);

        final List<ItemEnchantment> enchantments = itemMeta.getEnchants().entrySet().stream()
            .map(entry -> new ItemEnchantment(entry.getKey(), entry.getValue()))
            .collect(Collectors.toList());
        if (!enchantments.isEmpty()) {
            document.append("enchantments", enchantments.stream()
                .map(itemEnchantment -> new Document("type", itemEnchantment.getEnchantment().getName()).append("level", itemEnchantment.getLevel()))
                .collect(Collectors.toList()));
        }

        if (type == Material.WRITTEN_BOOK) {
            final BookMeta bookMeta = (BookMeta) itemMeta;
            final List<String> pages = bookMeta.getPages();
            final String author = bookMeta.getAuthor();
            final String title = bookMeta.getTitle();

            document.append("pages", pages).append("author", author).append("title", title);
        } else if (type == Material.SKULL_ITEM) {
            final SkullMeta skullMeta = (SkullMeta) itemMeta;
            final String owner = skullMeta.getOwner();

            document.append("owner", owner);
        } else if (type == Material.LEATHER_HELMET || type == Material.LEATHER_CHESTPLATE ||
            type == Material.LEATHER_LEGGINGS || type == Material.LEATHER_BOOTS) {
            final LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemMeta;
            final Color color = leatherArmorMeta.getColor();

            document.append("color-red", color.getRed()).append("color-green", color.getGreen()).append("color-blue", color.getBlue());
        } else if (type == Material.POTION) {
            final PotionMeta potionMeta = (PotionMeta) itemMeta;
            final PotionEffectType mainEffect = Potion.fromItemStack(itemStack).getType().getEffectType();
            document.append("main-effect", mainEffect.getName());
            document.append("potion-effects", potionMeta.getCustomEffects().stream()
                .map(potionEffect -> new Document("type", potionEffect.getType().getName())
                    .append("duration", potionEffect.getDuration())
                    .append("amplifier", potionEffect.getAmplifier())
                    .append("ambient", potionEffect.isAmbient())
                    .append("particles", potionEffect.hasParticles()))
                .collect(Collectors.toList()));
        }

        return document;
    }

    public ItemStack load(Document document) {
        /*
        type
        amount
        data
        durability
        flags
        display-name
        lore
        unbreakable
        enchantments (type, level)

        BOOK
        pages
        author
        title

        SKULL
        owner

        LEATHER ARMOR
        color (color-red, color-green, color-blue)

        POTION
        main-effect (type)
        potion-effects (type, duration, amplifier, ambient, particles)
         */

        final Material type = Material.valueOf(document.getString("type"));
        final int amount = document.getInteger("amount", 1);
        final short data = (short) document.getInteger("data", (short) 0);
        final short durability = (short) document.getInteger("durability", (short) 0);
        final List<ItemFlag> itemFlags = new ArrayList<>();
        final List flagDocument = document.get("flags", List.class);
        if (flagDocument != null) {
            for (ItemFlag itemFlag : ItemFlag.values())
                if (flagDocument.contains(itemFlag.name())) itemFlags.add(itemFlag);
        }
        final String displayName = document.getString("display-name");
        final List<String> lore = document.get("lore", List.class);
        final boolean unbreakable = document.getBoolean("unbreakable", false);
        final List<ItemEnchantment> enchantments = new ArrayList<>();
        if (document.containsKey("enchantments")) {
            document.get("enchantments", List.class).forEach(object -> {
                final Document enchantmentDocument = (Document) object;
                enchantments.add(new ItemEnchantment(Enchantment.getByName(enchantmentDocument.getString("type")), enchantmentDocument.getInteger("level")));
            });
        }

        final ItemStack itemStack = new ItemStack(type, amount, data);
        itemStack.setDurability(durability);
        final ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.addItemFlags(itemFlags.toArray(new ItemFlag[0]));
        itemMeta.setDisplayName(displayName);
        itemMeta.setLore(lore);
        itemMeta.spigot().setUnbreakable(unbreakable);
        enchantments.forEach(itemEnchantment -> itemMeta.addEnchant(itemEnchantment.getEnchantment(), itemEnchantment.getLevel(), true));

        if (type == Material.WRITTEN_BOOK) {
            final List<String> pages = document.get("pages", List.class);
            final String author = document.getString("author");
            final String title = document.getString("title");
            final BookMeta bookMeta = (BookMeta) itemMeta;
            bookMeta.setPages(pages);
            bookMeta.setAuthor(author);
            bookMeta.setTitle(title);
        } else if (type == Material.SKULL_ITEM) {
            final String owner = document.getString("owner");
            final SkullMeta skullMeta = (SkullMeta) itemMeta;
            skullMeta.setOwner(owner);
        } else if (type == Material.LEATHER_BOOTS || type == Material.LEATHER_CHESTPLATE ||
            type == Material.LEATHER_LEGGINGS || type == Material.LEATHER_HELMET) {
            final Color color = Color.fromRGB(
                document.getInteger("color-red"),
                document.getInteger("color-green"),
                document.getInteger("color-blue")
            );
            final LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemMeta;
            leatherArmorMeta.setColor(color);
        } else if (type == Material.POTION) {
            final PotionEffectType mainEffect = PotionEffectType.getByName(document.getString("main-effect"));
            final List<PotionEffect> potionEffects = new ArrayList<>();
            document.get("potion-effects", List.class).forEach(object -> {
                final Document potionDocument = (Document) object;
                potionEffects.add(new PotionEffect(
                    PotionEffectType.getByName(potionDocument.getString("type")),
                    potionDocument.getInteger("duration"),
                    potionDocument.getInteger("amplifier"),
                    potionDocument.getBoolean("ambient", true),
                    potionDocument.getBoolean("particles", true)
                ));
            });
            final PotionMeta potionMeta = (PotionMeta) itemMeta;
            potionMeta.setMainEffect(mainEffect);
            potionEffects.forEach(potionEffect -> potionMeta.addCustomEffect(potionEffect, true));
        }

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

}
