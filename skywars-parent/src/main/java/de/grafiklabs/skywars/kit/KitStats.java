package de.grafiklabs.skywars.kit;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Data
public class KitStats {

    private final UUID uuid;
    private String name;
    private final Map<StatsType, Integer> map = new HashMap<>();
    private final Map<StatsType, Integer> diff = new HashMap<>();

    public void increment(StatsType statsType, int amount) {
        this.diff.put(statsType, this.diff.getOrDefault(statsType, 0) + amount);
    }

    public void increment(StatsType statsType) {
        this.increment(statsType, 1);
    }

    public enum StatsType {

        KILLS,
        WINS,
        POINTS

    }

}
