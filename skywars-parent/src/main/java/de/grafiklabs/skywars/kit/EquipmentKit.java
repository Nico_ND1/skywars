package de.grafiklabs.skywars.kit;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;

/**
 * @author Nico_ND1
 */
public class EquipmentKit extends AbstractKit {

    @Getter private final Map<Integer, ItemStack> items;

    public EquipmentKit(String name, int price, ItemStack displayItem, Map<Integer, ItemStack> items, List<KitStats> topList) {
        super(name, price, displayItem, topList);
        this.items = items;
    }

    @Override
    public void onStart(Player player) {
        this.items.forEach((slot, itemStack) -> {
            switch (slot) {
                case 100:
                    player.getInventory().setBoots(itemStack);
                    break;
                case 101:
                    player.getInventory().setLeggings(itemStack);
                    break;
                case 102:
                    player.getInventory().setChestplate(itemStack);
                    break;
                case 103:
                    player.getInventory().setHelmet(itemStack);
                    break;
                default:
                    player.getInventory().setItem(slot, itemStack);
                    break;
            }
        });
    }
}
