package de.grafiklabs.skywars.kit;
import java.util.List;

/**
 * Interface to manage the {@link AbstractKit kits}.
 *
 * @author Nico_ND1
 */
public interface KitManager {

    /**
     * Get all {@link AbstractKit kits}.
     *
     * @return A {@link List<AbstractKit> list} with all {@link AbstractKit kits}
     */
    List<AbstractKit> getKits();

    /**
     * Get the default {@link AbstractKit}.
     *
     * @return The default {@link AbstractKit}
     */
    AbstractKit getDefaultKit();

    /**
     * Search for a {@link AbstractKit} where the {@link AbstractKit#name} equals to {@code name}.
     *
     * @param name The name
     * @return The found {@link AbstractKit} or if it doesn't exist, return {@code getDefaultKit}
     */
    AbstractKit getKit(String name);

}
