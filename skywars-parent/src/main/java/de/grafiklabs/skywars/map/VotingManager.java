package de.grafiklabs.skywars.map;
import de.grafiklabs.skywars.SkyWarsGame;
import de.grafiklabs.skywars.SkyWarsPlayer;

/**
 * Interface for votings.
 *
 * @author Nico_ND1
 */
public interface VotingManager {

    /**
     * Poll the {@link Island}.
     *
     * @param game          Instance for {@link SkyWarsGame}
     * @param islandManager Instance for {@link IslandManager}
     * @param middle        Whether the {@link Island} to poll should be {@link Island#middle}
     * @return The {@link Island} which has been polled
     */
    Island poll(SkyWarsGame game, IslandManager islandManager, boolean middle);

    /**
     * Get all votes for the given {@code island}.
     *
     * @param game   Instance for {@link SkyWarsGame}
     * @param island The {@link Island}
     * @return The amount of votes
     */
    int getVotes(SkyWarsGame game, Island island);

    /**
     * Give the {@link SkyWarsPlayer player's} vote for the {@code island}.
     *
     * @param player The {@link SkyWarsPlayer}
     * @param island The {@link Island}
     * @param middle Whether it's a middle is a middle {@link Island}
     * @return {@link Boolean#TRUE} if the {@code player player's} vote has been changed, or {@link Boolean#FALSE} if the {@code player} has already voted for the {@code island}
     */
    boolean vote(SkyWarsPlayer player, Island island, boolean middle);

}
