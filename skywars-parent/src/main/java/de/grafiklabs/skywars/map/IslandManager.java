package de.grafiklabs.skywars.map;
import org.bukkit.util.Vector;

import java.util.List;

/**
 * Interface to manage all {@link Island islands}.
 *
 * @author Nico_ND1
 */
public interface IslandManager {

    /**
     * Get all {@link Island islands}.
     *
     * @return A {@link List<Island> list} with all {@link Island islands}
     */
    List<Island> getIslands();

    /**
     * Get all {@link Island islands} where {@link Island#middle} is {@link Boolean#TRUE}.
     *
     * @return A {@link List<Island> list} with all {@link Island islands} where {@link Island#middle} is {@link Boolean#TRUE}
     */
    List<Island> getIslands(boolean middle);

    /**
     * Paste the middle {@link Island}.
     */
    void generateMiddle(Island middle, Island outer);

    /**
     * Calculate all {@link Island} {@link Vector vectors} and paste the {@code outer outer island}.
     *
     * @param middle  The {@link Island middle island}
     * @param outer   The {@link Island outer island}
     * @param islands Islands to generate
     * @param radius  Radius to the {@code middle middle island}
     * @return A {@link List<Vector> list} with all {@link Vector vectors} that have been calculated
     */
    List<Vector> generate(Island middle, Island outer, int islands, int radius);

}
