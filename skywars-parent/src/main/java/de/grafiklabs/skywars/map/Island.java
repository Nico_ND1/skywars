package de.grafiklabs.skywars.map;
import com.google.gson.*;
import com.google.gson.annotations.Expose;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.world.registry.WorldData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class Island implements JsonDeserializer<Island> {

    @Expose private final String name;
    private final Clipboard clipboard;
    @Expose private final Material material;
    @Expose private final int data;
    @Expose private final boolean middle;
    @Expose private final int radius;

    @Override
    public Island deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final File file = new File("plugins/SkyWars/Islands", jsonObject.get("name").getAsString() + ".schematic");
        try {
            final World world = Bukkit.getWorld("island");
            if (world == null) return null;
            final WorldData worldData = new BukkitWorld(world).getWorldData();
            final Clipboard clipboard = ClipboardFormat.SCHEMATIC.getReader(new FileInputStream(file)).read(worldData);

            return new Island(
                jsonObject.get("name").getAsString(),
                clipboard,
                Material.valueOf(jsonObject.get("material").getAsString()),
                jsonObject.get("data").getAsInt(),
                jsonObject.get("middle").getAsBoolean(),
                jsonObject.get("radius").getAsInt()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
