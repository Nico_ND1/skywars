package de.grafiklabs.skywars.inventory;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nico_ND1
 */
public class ClickInventory {

    private static final Map<Player, ClickInventory> INVENTORIES = new HashMap<>();

    public static ClickInventory getInventory(Player player) {
        return INVENTORIES.get(player);
    }

    static void unsetInventory(Player player) {
        INVENTORIES.remove(player);
    }

    protected final Player player;
    @Getter private final String title;
    private final Map<Integer, ClickItem> items = new HashMap<>();
    private final Inventory inventory;

    public ClickInventory(Player player, String title, int size) {
        this.player = player;
        this.title = title;
        this.inventory = Bukkit.createInventory(null, size, title);
    }

    public void setItem(int slot, ClickItem clickItem) {
        this.items.put(slot, clickItem);
        this.inventory.setItem(slot, clickItem.getItemStack());
        this.player.updateInventory();
    }

    void onClick(int slot) {
        final ClickItem clickItem = this.items.get(slot);
        if (clickItem != null) clickItem.onClick();
    }

    public void open() {
        this.player.openInventory(this.inventory);
        INVENTORIES.put(this.player, this);
    }

}
