package de.grafiklabs.skywars.inventory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Nico_ND1
 */
public class ClickInventoryListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null) return;

        final Player player = (Player) event.getWhoClicked();
        final ClickInventory inventory = ClickInventory.getInventory(player);
        if (inventory == null) return;
        if (!inventory.getTitle().equalsIgnoreCase(event.getClickedInventory().getName())) return;

        event.setCancelled(true);
        inventory.onClick(event.getSlot());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        ClickInventory.unsetInventory(event.getPlayer());
    }

}
