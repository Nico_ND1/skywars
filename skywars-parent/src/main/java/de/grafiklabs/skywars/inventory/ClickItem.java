package de.grafiklabs.skywars.inventory;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.inventory.ItemStack;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Data
public class ClickItem {

    private final ItemStack itemStack;
    private final Runnable runnable;

    void onClick() {
        if (this.runnable != null) this.runnable.run();
    }

}
