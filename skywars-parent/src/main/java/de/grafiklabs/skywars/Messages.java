package de.grafiklabs.skywars;
/**
 * @author Nico_ND1
 */
public enum Messages {

    COMPASS_ITEM_NAME(false, "§8» §aTeleporter"),
    LOBBY_PLAYERS_COUNTDOWN("§cEs werden noch §e{0} §cSpieler benötigt, damit die Runde starten kann."),
    JOIN("{0} §7hat das Spiel betreten."),
    QUIT("{0} §7hat das Spiel verlassen."),
    START_COMMAND_SUCCESS("Gestartet."),
    START_COMMAND_ALREADY_STARTED("Bereits gestartet."),
    START_COMMAND_USELESS("Du bist genauso unnötig wie diese Zeit\"verkürzung\"."),
    LOBBY_COUNTDOWN_START("Der Countdown wurde §agestartet§7."),
    LOBBY_COUNTDOWN_STOP("Der Countdown wurde §cgestoppt§7."),
    LOBBY_COUNTDOWN_RESET("Der Countdown wurde §czurückgesetzt§7."),
    LOBBY_COUNTDOWN_TICK("Das Spiel startet in §e{0}§7."),
    SERVER_STOP_TICK("Der Server §cstoppt §7in §e{0}§7."),
    GAME_PREPARE_COUNTDOWN_TICK("Die Schutzphase endet in §e{0}§7."),
    GAME_PREPARE_COUNTDOWN_FINISH("Die Schutzphase ist beendet."),
    KITS_ITEM(false, "Kits"),
    TEAMS_ITEM(false, "Teams"),
    KIT_INVENTORY(false, "Kits"),
    KIT_SELECT_INVENTORY(false, "Kit auswählen"),
    KIT_SELECT_ITEM(false, "Kit Auswählen"),
    KIT_SELECT_BACK(false, "Zurück"),
    KIT_SELECT_TOPLIST_ITEM(false, "Die besten Typen mit dem Kit"),
    KIT_SELECT_TOPLIST_LORE(false, "§6#{0} §f{1} §7({2} Kills, {3} Wins)"),
    KIT_BUY_ITEM(false, "Kit kaufen"),
    KIT_BUY_NOT_ENOUGH_COINS("§cNicht genug Coins."),
    KIT_BOUGHT("Kit gekauft."),
    KIT_SELECTED("Kit ausgewählt."),
    TEAM_ITEM(false, "Team #{0}"),
    TEAM_CREATE_ITEM(false, "Team erstellen oder leeres finden"),
    TEAM_FULL("§cDas Team ist voll."),
    TEAM_TITLE(false, "Teams"),
    LOBBY_VOTING_TITLE(false, "§8» §eInsel Voting"),
    VOTING_ITEM(false, "§8» §eInsel Voting §8«"),
    LOBBY_VOTING_ITEM(false, "§f{0}"),
    LOBBY_VOTING_LORE(false, "§7Votes§8: §6{0}"),
    LOBBY_VOTING_RESULT("Das Voting wurde beendet. Die mittlere Insel wird §e{0} §7sein. Die äußere §e{1}§7."),
    LOBBY_VOTING_VOTED("Du hast für die Insel §e{0} §7gestimmt."),
    LOBBY_VOTING_BOOK(false, "Äußere Inseln"),
    LOBBY_VOTING_BOOK_MIDDLE(false, "Mittlere Inseln"),
    NEXT_PAGE(false, "Nächste Seite"),
    PREVIOUS_PAGE(false, "Vorherige Seite"),
    PAGE_NOT_FOUND("§cDie Seite gibt es nicht."),
    KILL_KILLER("§c{0} §7wurde von §e{1} §7getötet."),
    KILL_NO_KILLER("§c{0} §7starb."),
    TEAMS_REMAINING("§e{0} §7Teams verbleiben."),
    WINNER_EMPTY("Niemand hat gewonnen!"),
    WINNERS("Team #{0} hat gewonnen! Spieler aus dem Team: {1}"),
    STATS_COMMAND_HELP("§cNutze bitte: /stats [Spieler]"),
    STATS_COMMAND_PLAYER_NOT_FOUND("§cDer Spieler wurde nicht gefunden."),
    STATS_COMMAND("Stats von §6{0}" +
        "\n§7Kills: §9{1}" +
        "\n§7Tode: §9{2}" +
        "\n§7K/D: §9{3}" +
        "\n§7Spiele: §9{4} §7(§e{5}% gewonnen§7)"),
    PLAYERS_PLAYER_NOT_INGAME("§cDer Spieler ist nicht mehr im Spiel."),
    PLAYERS_INVENTORY_TITLE(false, "Spieler"),
    PLAYERS_ITEM(false, "§7{0}"),
    SCOREBOARD_TITLE(false, "§dSKYWARS");

    private final String message;

    Messages(boolean prefix, String message) {
        if (prefix)
            this.message = "§8» §dSkyWars §8┃ §7" + message;
        else
            this.message = message;
    }

    Messages(String message) {
        this(true, message);
    }

    public String getMessage(Object... args) {
        String out = this.message.replace("%prefix%", "§8» §dSkyWars §8┃ §7");
        for (int i = 0; i < args.length; i++)
            out = out.replace("{" + i + "}", String.valueOf(args[i]));

        return out;
    }

}
