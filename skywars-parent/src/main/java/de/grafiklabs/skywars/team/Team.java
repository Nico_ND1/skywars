package de.grafiklabs.skywars.team;
import de.grafiklabs.skywars.SkyWarsPlayer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public class Team {

    private final int id;
    private final List<Location> spawns;
    private final List<SkyWarsPlayer> members = new ArrayList<>();

}
