package de.grafiklabs.skywars.team;
import de.grafiklabs.skywars.SkyWarsPlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;

/**
 * A interface to manage teams.
 *
 * @author Nico_ND1
 */
public interface TeamManager {

    /**
     * Get all {@link Team teams}.
     *
     * @return A {@link List<Team> list} with all {@link Team teams}
     */
    List<Team> getTeams();

    /**
     * Will search for an empty {@link Team} and return it. If there is no empty {@link Team}, it will create a new one.
     *
     * @return The {@link Team} found
     */
    Team findOrCreateEmptyTeam();

    /**
     * Will remove all empty {@link Team teams}.
     */
    void cleanUpEmptyTeams();

    /**
     * Will give every {@link SkyWarsPlayer} a team, which was either empty or just created.
     */
    void allocateTeamlessPlayers();

    /**
     * Search for a {@link Team} by the id.
     *
     * @param id The id of the {@link Team}
     * @return {@link Optional<Team>} which may contain the team found
     */
    Optional<Team> getTeam(int id);

    /**
     * Search for a {@link Team} where the give {@code player} is contained in {@link Team#members}.
     *
     * @param player The {@link Player} which is a member of a {@link Team}
     * @return {@link Optional<Team>} which may contain the team found
     */
    Optional<Team> getTeam(Player player);

    /**
     * Get all {@link SkyWarsPlayer players} from a {@link Team}.
     *
     * @param team The {@link Team}
     * @return A {@link List<SkyWarsPlayer> list} with all {@link SkyWarsPlayer members}
     */
    List<SkyWarsPlayer> getPlayers(Team team);

}
