package de.grafiklabs.skywars;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Interface for game stuff.
 *
 * @author Nico_ND1
 */
public interface SkyWarsGame {

    /**
     * Get the current {@link GameState}.
     *
     * @return The current {@link GameState}
     */
    GameState getGameState();

    /**
     * Set the current {@link GameState} to {@code newState}.
     *
     * @param newState The new {@link GameState}
     */
    void setGameState(GameState newState);

    /**
     * Get the cached {@link SkyWarsPlayer}.
     *
     * @param player The {@link Player bukkit player} to search for
     * @return The cached {@link SkyWarsPlayer}
     */
    SkyWarsPlayer getPlayer(Player player);

    /**
     * Unload the {@link SkyWarsPlayer} cached for the {@code player}. Will also save the {@link SkyWarsPlayer} to the {@link SkyWarsDatabase}.
     *
     * @param player The {@link Player bukkit player} to search for
     */
    void unloadPlayer(Player player);

    /**
     * Get all cached {@link SkyWarsPlayer players}.
     *
     * @return A {@link List<SkyWarsPlayer> list} with all {@link SkyWarsPlayer} players
     */
    List<SkyWarsPlayer> getPlayers();

    /**
     * Get all cached {@link SkyWarsPlayer players} that are currently not a spectator. Checked by {@link SkyWarsPlayer#isIngame()}.
     *
     * @return A {@link List<SkyWarsPlayer> list} with all {@link SkyWarsPlayer} players that are currently not a spectator
     */
    List<SkyWarsPlayer> getIngamePlayers();

    /**
     * Will end the game, if just one player or one team remains.
     */
    void checkEnd();

    /**
     * Give the {@code player} the spectator equipment and make him invisible for every {@link Player} on the server.
     *
     * @param player The {@link Player}
     * @return The spectator {@link Location}
     */
    Location giveSpectatorEquip(Player player);

    enum GameState {

        LOBBY,
        PREPARE,
        INGAME,
        END

    }

}
