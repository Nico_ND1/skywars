package de.grafiklabs.skywars;
import com.google.gson.*;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import de.grafiklabs.skywars.map.Island;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Data
public class SkyWarsConfig implements JsonDeserializer<SkyWarsConfig> {

    private static final Gson GSON = new GsonBuilder()
        .registerTypeAdapter(SkyWarsConfig.class, new SkyWarsConfig())
        .registerTypeAdapter(SerializableLocation.class, new SerializableLocation())
        .registerTypeAdapter(Island.class, new Island())
        .serializeNulls()
        .setPrettyPrinting()
        .excludeFieldsWithoutExposeAnnotation()
        .create();

    static SkyWarsConfig loadConfig() {
        final SerializableLocation emptyLocation = new SerializableLocation("world", 0, 0, 0, 0, 0);
        final SkyWarsConfig defaultConfig = new SkyWarsConfig(
            emptyLocation,
            2,
            60,
            1,
            Arrays.asList(
                new Island(
                    "test",
                    null,
                    Material.WOOL,
                    0,
                    false,
                    50
                ),
                new Island(
                    "test middle",
                    null,
                    Material.WOOL,
                    1,
                    true,
                    0
                )
            )
        );
        final File file = new File("plugins/SkyWars", "config.json");
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            saveConfig(defaultConfig);
        }

        try {
            return GSON.fromJson(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")), SkyWarsConfig.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return defaultConfig;
    }

    public static void saveConfig(SkyWarsConfig config) {
        try (FileWriter fileWriter = new FileWriter(new File("plugins/SkyWars", "config.json"))) {
            fileWriter.write(GSON.toJson(config, SkyWarsConfig.class));
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Expose private final SerializableLocation lobbyLocation;
    @Expose private final int neededPlayersToStart;
    @Expose private final int initialCountdown;
    @Expose private final int teamSize;
    @Expose private final List<Island> islands;

    @Override
    public SkyWarsConfig deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject object = jsonElement.getAsJsonObject();

        return new SkyWarsConfig(
            context.deserialize(object.get("lobbyLocation"), SerializableLocation.class),
            object.get("neededPlayersToStart").getAsInt(),
            object.get("initialCountdown").getAsInt(),
            object.get("teamSize").getAsInt(),
            GSON.fromJson(object.get("islands"), new TypeToken<List<Island>>() {
            }.getType())
        );
    }

    @AllArgsConstructor
    @NoArgsConstructor(force = true)
    @Data
    public static class SerializableLocation implements JsonDeserializer<SerializableLocation> {

        @Expose private final String world;
        @Expose private final double x;
        @Expose private final double y;
        @Expose private final double z;
        @Expose private final float yaw;
        @Expose private final float pitch;

        public Location toLocation() {
            return new Location(Bukkit.getWorld(this.world), this.x, this.y, this.z, this.yaw, this.pitch);
        }

        @Override
        public SerializableLocation deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final JsonObject jsonObject = jsonElement.getAsJsonObject();
            return new SerializableLocation(
                jsonObject.get("world").getAsString(),
                jsonObject.get("x").getAsDouble(),
                jsonObject.get("y").getAsDouble(),
                jsonObject.get("z").getAsDouble(),
                jsonObject.get("yaw").getAsFloat(),
                jsonObject.get("pitch").getAsFloat()
            );
        }
    }

}
