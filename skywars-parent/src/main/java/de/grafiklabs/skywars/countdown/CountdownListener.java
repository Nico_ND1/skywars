package de.grafiklabs.skywars.countdown;
/**
 * @author Nico_ND1
 */
public interface CountdownListener {

    void onStart();

    void onStop();

    void onReset();

    void onFinish();

    void onTick(int previousTick);

}
