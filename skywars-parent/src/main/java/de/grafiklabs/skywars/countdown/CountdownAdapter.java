package de.grafiklabs.skywars.countdown;
import lombok.Setter;

/**
 * @author Nico_ND1
 */
public class CountdownAdapter implements CountdownListener {

    @Setter protected Countdown countdown;

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onReset() {
    }

    @Override
    public void onFinish() {
    }

    @Override
    public void onTick(int previousTick) {
    }
}
