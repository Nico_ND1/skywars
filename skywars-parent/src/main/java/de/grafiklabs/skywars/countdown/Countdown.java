package de.grafiklabs.skywars.countdown;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

/**
 * @author Nico_ND1
 */
public final class Countdown implements Runnable {

    private final Plugin plugin;
    private final int seconds;
    @Getter private final CountdownListener countdownListener;
    private int taskId;
    @Getter private int currentTick;

    public Countdown(Plugin plugin, int seconds, CountdownListener countdownListener) {
        this.plugin = plugin;
        this.seconds = seconds;
        this.countdownListener = countdownListener;
        if (countdownListener instanceof CountdownAdapter) {
            ((CountdownAdapter) countdownListener).countdown = this;
        }
        this.currentTick = seconds;
    }

    public final boolean start() {
        if (this.taskId == 0) {
            this.taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(this.plugin, this, 20, 20);

            this.countdownListener.onStart();
            return true;
        }
        return false;
    }

    public final boolean stop() {
        if (this.taskId != 0) {
            Bukkit.getScheduler().cancelTask(this.taskId);
            this.taskId = 0;

            this.countdownListener.onStop();
            return true;
        }
        return false;
    }

    public final void reset() {
        this.currentTick = this.seconds;

        this.countdownListener.onReset();
    }

    public final void setTime(int newTime) {
        this.currentTick = newTime;
    }

    public final boolean isStarted() {
        return this.taskId != 0;
    }

    @Override
    public final void run() {
        if (this.currentTick == 0) {
            Bukkit.getScheduler().cancelTask(this.taskId);
            this.taskId = 0;

            this.countdownListener.onFinish();
            return;
        }

        this.countdownListener.onTick(this.currentTick);

        this.currentTick--;
    }
}
