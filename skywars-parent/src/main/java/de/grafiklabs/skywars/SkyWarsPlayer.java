package de.grafiklabs.skywars;
import de.grafiklabs.skywars.kit.AbstractKit;
import de.grafiklabs.skywars.kit.KitStats;
import de.grafiklabs.skywars.map.Island;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Contains all stats and more stuff used for the game.
 *
 * @author Nico_ND1
 */
@AllArgsConstructor
@Data
public class SkyWarsPlayer {

    private final Player player;
    private final List<String> kits;
    private SkyWarsStatsPlayer statsPlayer;
    private AbstractKit lastKit;
    private AbstractKit kit;
    private KitStats kitStats;
    private int coins;
    private int coinsDiff;
    private Island votedMiddleIsland;
    private Island votedIsland;
    private boolean ingame;
    private SkyWarsPlayer lastDamager;

    public void setKit(AbstractKit kit) {
        this.lastKit = kit;
        this.kit = kit;

        this.player.getInventory().setItem(8, kit.getDisplayItem());
    }

}
